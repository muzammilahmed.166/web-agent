import { ACTION_GET_CUSTOMER_CREATE_SLOT_REQ, ACTION_GET_CUSTOMER_VCIP_DETAILS_REQ, ACTION_GET_CUSTOMER_VCIP_DETAILS_RES } from "./SagaActionTypes"


export const actionGetVcipDetails = (payload) => {
    return {
        type: ACTION_GET_CUSTOMER_VCIP_DETAILS_REQ,
        payload: payload
    }
}

export const actionGetVcipDetailsResponse = (payload) => {
    return {
        type: ACTION_GET_CUSTOMER_VCIP_DETAILS_RES,
        payload: payload
    }
}

export const actionCreateSlot = (payload) => {
    return {
        type: ACTION_GET_CUSTOMER_CREATE_SLOT_REQ,
        payload: payload
    }
}