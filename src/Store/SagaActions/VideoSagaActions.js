import {
    ACTION_CREATE_VIDEO_SESSION_REQ,
    ACTION_CREATE_VIDEO_TOKEN_RECORD_REQ,
    ACTION_CREATE_VIDEO_TOKEN_REQ,
    ACTION_CREATE_VIDEO_TOKEN_RES,
    ACTION_END_VIDEO_REQ,
    ACTION_UPDATE_VCIP_STATUS_REQ
} from "./SagaActionTypes"

export const createVideoSessionSagaAction = (payload) => {
    return {
        type: ACTION_CREATE_VIDEO_SESSION_REQ,
        payload: payload
    }
}

export const createVideoTokenSagaAction = (payload) => {
    return {
        type: ACTION_CREATE_VIDEO_TOKEN_REQ,
        payload: payload
    }
}

export const createVideoTokenForRecordingSagaAction = (payload) => {
    return {
        type: ACTION_CREATE_VIDEO_TOKEN_RECORD_REQ,
        payload: payload
    }
}

export const createVideoTokenSagaActionResponse = (payload) => {
    return {
        type: ACTION_CREATE_VIDEO_TOKEN_RES,
        payload: payload
    }
}

export const endVideoCallSagaAction = (payload) => {
    return {
        type: ACTION_END_VIDEO_REQ,
        payload: payload
    }
}

export const updateVcipStatusSagaAction = (payload) => {
    return {
        type: ACTION_UPDATE_VCIP_STATUS_REQ,
        payload: payload
    }
}