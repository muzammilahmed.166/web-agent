import { ACTION_GET_PAN_CAPTURE_REQ, ACTION_GET_PAN_SAVEDETAILS_REQ, ACTION_GET_PAN_VERIFY_REQ } from "./SagaActionTypes";


export const actionPanCapture = (payload) => {
    return {
        type: ACTION_GET_PAN_CAPTURE_REQ,
        payload: payload
    }
}

export const actionSavePanDetails = (payload) => {
    return {
        type: ACTION_GET_PAN_SAVEDETAILS_REQ,
        payload: payload
    }
}

export const actionVerifyPanNumber = (payload) => {
    return {
        type: ACTION_GET_PAN_VERIFY_REQ,
        payload: payload
    }
}
