import toast from 'react-hot-toast';
import { put, takeLatest, call } from 'redux-saga/effects';
import Axios from '../../Service/axios'
import { actionReqResStatusLoaderSagaAction } from '../SagaActions/CommonSagaActions';
import { ACTION_GET_DIGILOCKER_LINK_REQ, ACTION_GET_DIGILOCKER_STATUS_REQ } from '../SagaActions/SagaActionTypes';

// GET DIGILOCKER URL
const getDigiLocckerReq = (model) => {
    const URL = "DigiLockerRequest";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDigiLockerReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDigiLocckerReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
            const data = { url: resp?.url, txnid: resp?.txnid };
            if (action?.payload?.callback) {
                action?.payload?.callback(data);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// GET DIGILOCKER STATUS
const getDigiLocckerStatusReq = (model) => {
    const URL = "FetchDigiLockerTxnStatus";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDigiLockerStatusReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDigiLocckerStatusReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
            if (resp?.statuscode === '1' && action?.payload?.callback) {
                action?.payload?.callback(resp?.kycdetails);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

export default function* AdharWatcherSaga() {
    yield takeLatest(ACTION_GET_DIGILOCKER_LINK_REQ, getDigiLockerReqSaga);
    yield takeLatest(ACTION_GET_DIGILOCKER_STATUS_REQ, getDigiLockerStatusReqSaga);
}