import toast from 'react-hot-toast';
import { put, takeLatest, call } from 'redux-saga/effects';
import Axios from '../../Service/axios';
import AxiosFormData from '../../Service/axiosFormData';
import {
    ACTION_KYC_AADHAR_OFFLINE, ACTION_KYC_FACEMATCH_CHECK_REQ, ACTION_KYC_GET_QUESTIONS,
    ACTION_KYC_MATCH_AADHAAR_CHECK_REQ, ACTION_KYC_MATCH_AADHAAR_PAN_CHECK_REQ, ACTION_KYC_SUBMIT_QUESTION
} from '../SagaActions/actionTypes';
import { actionReqResStatusLoaderSagaAction } from '../SagaActions/CommonSagaActions';
import { aadharOfflineKycResponseSagaAction } from '../SagaActions/KYCProcessSagaAction';
import { ACTION_POST_UPLOAD_SCREEN_RECORDREQ } from '../SagaActions/SagaActionTypes';

// GET QUESTIONS
const getQuestionsReq = () => {
    const model = {
        vcipkey: sessionStorage.getItem('vcipkey')
    }
    const URL = "GetQuestions";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getQuestionsReqSaga(action) {
    // yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getQuestionsReq);
        if (resp && resp?.respcode === "200") {
            action?.payload?.callback(resp?.questions);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        // yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// SUBMIT QUESTION
const submitQuestionReq = (model) => {
    const URL = "SubmitQuestions";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* submitQuestionReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(submitQuestionReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            action?.payload?.callback({
                ...resp,
                status: action?.payload?.model?.status,
                quesid: action?.payload?.model?.quesid
            });
            toast.success(resp?.respdesc);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// SUBMIT QUESTION
const aadharOfflineMatchReq = (model) => {
    const URL = "CheckImageLiveness";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* aadharOfflineMatchReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(aadharOfflineMatchReq, action?.payload?.model);
        action?.payload?.callback(resp);
        if (resp && resp?.respcode === "200") {
            // yield put(aadharOfflineKycResponseSagaAction(resp))
            toast.success(resp?.respdesc);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// UPDATE MATCH STATUS BY AGENT
const updateMatchStatusByAgentReq = (model) => {
    const URL = "UpdateMatchStatusByAgent";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* updateMatchStatusByAgentReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(updateMatchStatusByAgentReq, action?.payload?.model);
        action?.payload?.callback(resp, action?.payload?.model?.matchstatus);
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// SUBMIT QUESTION
const facematchCheckReq = (model) => {
    const URL = "FaceMatch";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* facematchCheckReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(facematchCheckReq, action?.payload?.model);
        action?.payload?.callback(resp);
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// SUBMIT QUESTION
const uploadScreenRecordVideoReq = (model) => {
    const URL = "UploadVCIPRecordedVideo";
    // const headers = { headers: { "Content-Type": "multipart/form-data" } }
    return AxiosFormData.post(URL, model).then(res => { return res?.data })
}

function* uploadScreenRecordVideoReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(uploadScreenRecordVideoReq, action?.payload?.model);
        // action?.payload?.callback(resp);
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}


export default function* KYCProcessWatcherSaga() {
    yield takeLatest(ACTION_KYC_GET_QUESTIONS, getQuestionsReqSaga);
    yield takeLatest(ACTION_KYC_SUBMIT_QUESTION, submitQuestionReqSaga);
    yield takeLatest(ACTION_KYC_AADHAR_OFFLINE, aadharOfflineMatchReqSaga);
    yield takeLatest(ACTION_KYC_MATCH_AADHAAR_PAN_CHECK_REQ, updateMatchStatusByAgentReqSaga);
    yield takeLatest(ACTION_KYC_FACEMATCH_CHECK_REQ, facematchCheckReqSaga);
    yield takeLatest(ACTION_POST_UPLOAD_SCREEN_RECORDREQ, uploadScreenRecordVideoReqSaga);
}