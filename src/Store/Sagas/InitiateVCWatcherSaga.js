import toast from 'react-hot-toast';
import { put, takeLatest, call, takeEvery } from 'redux-saga/effects';
import Axios from '../../Service/axios'
import { actionReqResStatusLoaderSagaAction } from '../SagaActions/CommonSagaActions';
import {
    ACTION_GET_VC_INITIATE_CONFERENCE_QUEUE_REQ,
    ACTION_GET_VC_JOIN_VIDEO_SESSION_REQ,
    ACTION_GET_VC_LANGUAGES_REQ,
    ACTION_GET_VC_RESCHEDULE_REQ,
    ACTION_GET_VC_SCHEDULE_CALENDER_REQ,
    ACTION_GET_VC_SCHEDULE_CANCEL_REQ,
    ACTION_GET_VC_SCHEDULE_DETAILS_REQ,
    ACTION_GET_VC_UPDATED_TOKEN_REQ
} from '../SagaActions/SagaActionTypes';

// GET LANGUAGES
const getLanguagesReq = (body) => {
    const URL = 'GetLanguages';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getLangaugesReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getLanguagesReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp?.languages);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// INITIATE CONFERENCE QUEUE
const getInitiateConferenceQueueReq = (body) => {
    const URL = 'InitiateVideoConferenceQueue';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getInitiateConferenceQueueReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getInitiateConferenceQueueReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// GET UPDATED TOKEN
const getUpdatedTokenReq = (body) => {
    const URL = 'GetUpdatedVCIPIDTokenNumber';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getUpdatedTokenReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getUpdatedTokenReq, action?.payload?.model);
        if (action?.payload?.callback) {
            action?.payload?.callback(resp);
        }
        if (resp && resp?.respcode !== "200") {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// RESCHEDULE VIDEO CALL
const getRescheduleVcReq = (body) => {
    const URL = 'CreateVideoCallSchedule';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getRescheduleVcReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getRescheduleVcReq, action?.payload?.model);
        if (action?.payload?.callback) {
            action?.payload?.callback(resp);
        }
        if (resp && resp?.respcode === "200") {
            toast.success(resp?.respdesc);
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// GET SCHEDULE CALENDER
const getScheduleCalenderVcReq = (body) => {
    const URL = 'GetVideoCallScheduleCalender';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getScheduleCalenderVcReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getScheduleCalenderVcReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// GET SCHEDULE DETAILS
const getScheduleDetailsVcReq = (body) => {
    const URL = 'GetVideoCallScheduleDetails';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getScheduleDetailsVcReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getScheduleDetailsVcReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// CANCEL SCHEDULE DETAILS
const CancelScheduleVcReq = (body) => {
    const URL = 'CancelVideoCallSchedule';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* getScheduleCancelVcReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(CancelScheduleVcReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// JOIN VIDEO SESSION
const joinVideoSessionReq = (body) => {
    const URL = 'JoinVideoConferenceSessionID';
    return Axios.post(URL, body).then(res => { return res?.data })
}

function* joinVideoSessionReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(joinVideoSessionReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(action?.payload?.vcipID);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

export default function* InitiateVCWatcherSaga() {
    yield takeLatest(ACTION_GET_VC_LANGUAGES_REQ, getLangaugesReqSaga);
    yield takeLatest(ACTION_GET_VC_INITIATE_CONFERENCE_QUEUE_REQ, getInitiateConferenceQueueReqSaga);
    yield takeEvery(ACTION_GET_VC_UPDATED_TOKEN_REQ, getUpdatedTokenReqSaga);
    yield takeLatest(ACTION_GET_VC_RESCHEDULE_REQ, getRescheduleVcReqSaga);
    yield takeLatest(ACTION_GET_VC_SCHEDULE_CALENDER_REQ, getScheduleCalenderVcReqSaga);
    yield takeEvery(ACTION_GET_VC_SCHEDULE_DETAILS_REQ, getScheduleDetailsVcReqSaga);
    yield takeLatest(ACTION_GET_VC_SCHEDULE_CANCEL_REQ, getScheduleCancelVcReqSaga);
    yield takeLatest(ACTION_GET_VC_JOIN_VIDEO_SESSION_REQ, joinVideoSessionReqSaga);
}