import toast from 'react-hot-toast';
import { put, takeLatest, call, takeEvery } from 'redux-saga/effects';
import Axios from '../../Service/axios'
import { ACTION_GET_CALL_HISTORY_REQ, ACTION_GET_DASHBOARD_INFO_REQ, ACTION_GET_DASHBOARD_LIVE_REQ, ACTION_GET_DASHBOARD_SCHEDULE_REQ, ACTION_GET_ROLES_REQ, ACTION_POST_LOGIN_REQ } from '../SagaActions/actionTypes';
import { actionReqResStatusLoaderSagaAction, getDashboardVCIPCountSagaAction } from '../SagaActions/CommonSagaActions';

// DASHABORD INFO
const getDashboardInfoReq = (model) => {
    const URL = "GetDashBoardCount";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardInfoReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardInfoReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// DASHBOARD LIVE
const getDashboardLiveReq = (model) => {
    const URL = "AgentVcipIdList";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardLiveReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardLiveReq, action?.payload?.model);
        if (resp) {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp);
                const payload = {
                    listtype: 1,
                    count: resp?.vciplistcount
                }
                yield put(getDashboardVCIPCountSagaAction(payload))
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// DASHBOARD SCHEDULE
const getDashboardScheduleReq = (model) => {
    const URL = "AgentVcipIdList";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getDashboardScheduleReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getDashboardScheduleReq, action?.payload?.model);
        if (resp) {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp?.vciplist);
                const payload = {
                    listtype: 2,
                    count: resp?.vciplistcount
                }
                console.log(payload, 'adp');
                yield put(getDashboardVCIPCountSagaAction(payload))

            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}

// DASHBOARD CALL HISTORY
const getCallHistoryReq = (model) => {
    const URL = "AgentCallHistory";
    return Axios.post(URL, model).then(res => { return res?.data })
}

function* getCallHistoryReqSaga(action) {
    yield put(actionReqResStatusLoaderSagaAction(true));
    try {
        const resp = yield call(getCallHistoryReq, action?.payload?.model);
        if (resp && resp?.respcode === "200") {
            if (action?.payload?.callback) {
                action?.payload?.callback(resp?.callhistory);
            }
        } else {
            toast.error(resp?.respdesc);
        }
    } catch (err) {
        if (err.response) {
            toast.error(err?.response?.data?.errors?.length && err?.response?.data?.errors[0]?.message);
        } else {
            toast.error(err.message);
        }
    } finally {
        yield put(actionReqResStatusLoaderSagaAction(false));
    }
}


export default function* AgentDashboardWatcherSaga() {
    yield takeLatest(ACTION_GET_DASHBOARD_INFO_REQ, getDashboardInfoReqSaga);
    yield takeEvery(ACTION_GET_DASHBOARD_LIVE_REQ, getDashboardLiveReqSaga);
    yield takeEvery(ACTION_GET_DASHBOARD_SCHEDULE_REQ, getDashboardScheduleReqSaga);
    yield takeEvery(ACTION_GET_CALL_HISTORY_REQ, getCallHistoryReqSaga);
}