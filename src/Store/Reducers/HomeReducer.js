import * as SagaActionTypes from '../SagaActions/SagaActionTypes'
import * as actionTypes from '../SagaActions/actionTypes'

const initial = {
    isLoading: false,
    apiStatus: 0,
    liveListCount: 0,
    scheduleListCount: 0,
    updateStatusbarList: {
        questions: false,
        captureFace: false,
        checkLocation: false,
        checkAadhar: false,
        checkPAN: false,
        instructions: false,
        // signature: false,
        isCallEnded: false,
    }
}

const HomeReducer = (state = initial, action) => {
    switch (action.type) {
        case SagaActionTypes.APISTATUS:
            if (action.payload) {
                return { ...state, apiStatus: state.apiStatus + 1 }
            } else {
                return { ...state, apiStatus: state.apiStatus - 1 }
            }
        case SagaActionTypes.ACTION_GET_DASHBOARD_VCIPID_COUNT_REQ:
                if (action.payload.listtype == "1") {
                    return { ...state, liveListCount: action.payload.count }
                } else {
                    return { ...state, scheduleListCount: action.payload.count }
                }

        case actionTypes.ACTION_GET_STATUSBAR_UPDATE_STATUS:
            return { ...state, updateStatusbarList: { ...state.updateStatusbarList, ...action.payload } }

        case actionTypes.ACTION_END_VIDEO_CALL_BY_AGENT_REQ:
            return { ...state, isCallEnded: action.payload }

        default:
            return state;
    }
}

export default HomeReducer;