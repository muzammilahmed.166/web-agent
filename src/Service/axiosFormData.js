import axios from "axios";

const instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL
});

instance.defaults.headers.common['Content-Type'] = "multipart/form-data";
instance.defaults.headers.common['apikey'] = "0";

instance.interceptors.request.use(
    request => {
        const authkey = sessionStorage.getItem('authkey');
        request.headers.common["authkey"] = authkey;
        // if (authkey) {
        // }

        return request;
    },
    error => {
        return Promise.reject(error);
    }
);


instance.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        return Promise.reject(error);
    }
);


export default instance;