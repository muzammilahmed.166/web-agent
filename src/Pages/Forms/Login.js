import React, { useEffect, useState } from 'react'
import toast from 'react-hot-toast';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import base64 from 'base-64';
import LoginCmp from '../../Components/FormCmp/LoginCmp';
import RouteNames from '../../Constants/RouteNames';
import { getRolesSagaAction, loginSagaAction } from '../../Store/SagaActions/AuthSagaActions';
const $ = window.$;

const Login = () => {
    const [loginObj, setLoginObj] = useState({
        username: '',
        password: '',
    });
    const [roleObj, setRoleObj] = useState({});
    const [roles, setRoles] = useState([]);
    const [isLoading, setIsLoading] = useState(false);


    const naviage = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        sessionStorage.clear();
        dispatch(getRolesSagaAction({ callback: getRolesData }));
    }, []);

    const getRolesData = (data) => {
        setRoles(data)
    }


    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === 'rolename') {
            const role = value.split('-')
            const roleObjData = {
                rid: role[0],
                rolename: role[1],
            }
            setRoleObj(roleObjData);
        } else {
            setLoginObj({ ...loginObj, [name]: value })
        }
    }

    const login = (e) => {
        e.preventDefault();
        // if (loginObj.username && loginObj.password && roleObj?.rid && roleObj?.rolename) {
        if (loginObj.username && loginObj.password) {
            const encodePwd = base64.encode(loginObj?.password);
            const model = {
                username: loginObj.username,
                password: encodePwd,
                rolename: "Agent",
                // rolename: roleObj?.rolename,
                // rid: roleObj.rid,
                rid: "3",
                authflag: '1'
            }

            setIsLoading(!isLoading);
            dispatch(loginSagaAction({ model: model, callback: loginRespData }))


        } else {
            toast.error('Please check all inputs')
        }
    }
    const validationyes = () => {
        naviage(RouteNames.DASHBOARD_LIVE);

    }
    const validationno = () => {
        naviage(RouteNames.LOGIN);
        window.location.reload();
    }



    const loginRespData = (data) => {
        setIsLoading(isLoading);

        if (data.authflag === "1") {
            $('#login').modal('hide');
        }
        if (data.respcode == "200") {
            setTimeout(() => {
                setIsLoading(!isLoading);

            }, 5000);

            naviage(RouteNames.DASHBOARD_LIVE);
            toast.success(data.respdesc)
            sessionStorage.setItem('username', data?.username)
        }
        else if (data.respcode == "4003") {
            toast.error(data.respdesc)
            naviage(RouteNames.LOGIN);
        } else if (data.respcode == "468") {
            $('#login').modal('show');

            naviage(RouteNames.LOGIN);
            // toast.error(data.respdesc)
        } else if (data.respcode == "4002") {
            naviage(RouteNames.LOGIN);
            toast.error(data.respdesc)
        } else {
            toast.error(data.respdesc)

        }
        // naviage(RouteNames.DASHBOARD_LIVE);
        // toast.success(data.respdesc)

    }

    return (
        <>
            <LoginCmp
                roles={roles}
                handleChange={handleChange}
                login={login}
                isLoading={isLoading}
                validationyes={validationyes}
                validationno={validationno}
            />

        </>

    )
}

export default Login