import React from 'react'
import { useNavigate } from 'react-router-dom';
import AadhaarUploadSuccessCmp from '../../../Components/SelectKYCProcessCmp/AadhaarUploadSuccessCmp'
import RouteNames from '../../../Constants/RouteNames';
import AppFooter from '../../Common/AppFooter'

const AadhaarUploadSuccess = () => {

    let navigate = useNavigate();

    const nextPage = () => {
        navigate(RouteNames.INITIATE_VIDEO_CALL);
    }

    return (
        <>
            <AadhaarUploadSuccessCmp />
            <AppFooter
                btnName="Proceed"
                navigate={nextPage}
            />
        </>
    )
}

export default AadhaarUploadSuccess