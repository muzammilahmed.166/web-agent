import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AadhaarUploadCmp from '../../../Components/SelectKYCProcessCmp/AadhaarUploadCmp'
import RouteNames from '../../../Constants/RouteNames'
import PortalModal from '../../../Portals/PortalModal';
import AppFooter from '../../Common/AppFooter'
import AadhaarNumberModal from '../../Modals/AadhaarNumberModal';
import AadhaarNumberVerifyModal from '../../Modals/AadhaarNumberVerifyModal';

const AadhaarUpload = () => {


  const [isOpen, setIsOpen] = useState(false);
  const [isVerify, setIsVerify] = useState(false);

  let navigate = useNavigate();

  const openModal = () => {
      setIsOpen(true);
  }

  const generateXMLModal = () => {
      setIsOpen(true);
  }

  const sendOTPModal = () => {
    setIsOpen(false);
    // setTimeout(() => {
      setIsVerify(true);
    // }, 1000);
  }

  const verifyOTP = () => {
    setIsVerify(false);
    navigate(RouteNames.AADHAR_KYC_PROCESS_DOWNLOAD);
  }
  
  const nextPage = () => {
    navigate(RouteNames.AADHAR_KYC_PROCESS_DOWNLOAD);
  }

  const closeModal = () => {
      setIsOpen(false);
      setIsVerify(false);
  }

  return (
    <>
      <AadhaarUploadCmp />
      <AppFooter
        btnName="Proceed"
        isGenerate={true}
        generateXMLModal={generateXMLModal}
        navigate={nextPage}
      />

      <PortalModal isOpen={isOpen} isBottom={true}>
        <AadhaarNumberModal sendOTPModal={sendOTPModal} closeModal={closeModal} />
      </PortalModal>

      <PortalModal isOpen={isVerify} isBottom={true}>
        <AadhaarNumberVerifyModal verifyOTP={verifyOTP} closeModal={closeModal} />
      </PortalModal>
    </>
  )
}

export default AadhaarUpload;