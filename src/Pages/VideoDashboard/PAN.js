import React, { useContext, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import PANCheckCmp from '../../Components/VideoCallCmp/PANCheckCmp';
import { statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import { faceMatchSagaAction, updateMatchStatusByAgentSagaAction } from '../../Store/SagaActions/KYCProcessSagaAction';
import { VCIPDetailsContext } from './VideoDashboard';

const PAN = () => {
  const { customerVcipDetails, navigate, vcipid, img } = useContext(VCIPDetailsContext);

  const [isMatchChecked, setIsMatchChecked] = useState(false);
  const [facematchDetails, setFacematchDetails] = useState({});
  const [isSelected, setIsSelected] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();
  // const aadhaarOfflineMatchData = useSelector(state => state?.VcipReducer?.aadhaarOfflineMatchData)
  const vcipkey = sessionStorage.getItem('vcipkey');

  useEffect(() => {
    const base64Img = img?.split(',')[1];
    const model = {
      vcipkey: vcipkey,
      image1: customerVcipDetails?.kycdetails?.pht,
      image2: base64Img,
      matchtype: '3', //3- pan live
      rrn: '1'
    }
    dispatch(faceMatchSagaAction({ model: model, callback: getFaceMatchDetails }))
  }, []);

  const getFaceMatchDetails = (data) => {
    setFacematchDetails(data);
  }

  const updateFacematchStatus = (val) => {
    const model = {
      vcipkey: vcipkey,
      matchtype: '2', //2-  pan live
      matchstatus: val
    }
    dispatch(updateMatchStatusByAgentSagaAction({ model: model, callback: updateFaceMatchRes }))
  }

  const updateFaceMatchRes = (data, type) => {
    setIsLoading(false);
    if (data?.respcode === "200") {
      setIsLoading(true);
      setIsMatchChecked(true);
      setIsSelected(type);
    } else {
      setIsLoading(false);
      setIsMatchChecked(false);
      setIsSelected('');
    }
  }


  const matchPanWithLive = () => {
    setIsMatchChecked(true);
  }

  const nextPage = () => {
    dispatch(statusbarUpdateSagaAction({ checkPAN: true }));
    // dispatch(statusbarUpdateSagaAction({ checkAadhar: true }));

    navigate(`/video/${vcipid}/pan-details-check`)
  }

  return (
    <>
      <PANCheckCmp
        facematchDetails={facematchDetails}
        customerVcipDetails={customerVcipDetails}
        img={img}
        isMatchChecked={isMatchChecked}
        isSelected={isSelected}
        isLoading = {isLoading}
        updateFacematchStatus={updateFacematchStatus}
        // aadhaarOfflineMatchData={aadhaarOfflineMatchData}
        nextPage={nextPage}
      />
    </>
  )
}

export default PAN