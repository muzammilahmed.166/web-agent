import React, { useContext, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import QuestionsCmp from '../../Components/VideoCallCmp/QuestionsCmp'
import { pushNotificationSaga, statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import {
  getQuestionsSagaAction,
  SubmitQuestionSagaAction
} from '../../Store/SagaActions/KYCProcessSagaAction';
import Header from '../Common/Header';
import { VCIPDetailsContext } from './VideoDashboard';

const Questions = () => {
  const { navigate, vcipid } = useContext(VCIPDetailsContext);

  const [questionsList, setQuestionsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isDisabled, setISDisabled] = useState(true);
  var lastItem = null;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getQuestionsSagaAction({ callback: getQuestionsData }))
  }, [])

  const getQuestionsData = (data) => {
    const modifiedRes = data?.map(item => {
      return {
        ...item,
        isAsk: false,
        isAnwsered: false,
        remarks: ''
      }
    })
    setQuestionsList(modifiedRes);
  }

  // HANDLE CHANGE FOR REMORKS
  const handleRemorks = (event, data, i) => {
    const { value } = event.target;
    let questionsListCopy = [...questionsList];
    questionsListCopy[i].remarks = value;
    setQuestionsList(questionsListCopy);
  }

  // ASK QUESTION
  const askQuestion = (qtnData) => {
    pushNotificationRespData(qtnData?.ques)
    const updatedQtns = questionsList.map(item => {
      if (item?.quesid === qtnData?.quesid) {
        return {
          ...item,
          isAsk: true
        }
      }
      return item
    });
    setQuestionsList(updatedQtns);
    setISDisabled(true);

  }

  // SUBMIT ANSWER
  const submitQuestion = (data, status) => {
    const model = {
      vcipkey: sessionStorage.getItem('vcipkey'),
      quesid: data?.quesid,
      time: new Date().toLocaleTimeString(),
      status: status,
      remarks: data?.remarks
    }
    setIsLoading(true);
    dispatch(SubmitQuestionSagaAction({ model: model, callback: getSubmittedRes }));
  }

  // UPDATE ANSWERED STATUS
  const getSubmittedRes = (data) => {
    const updatedQtns = questionsList.map((item, i) => {
      lastItem = item?.quesid;
      if (item?.quesid === data?.quesid) {
        return {
          ...item,
          isAsk: true,
          isAnwsered: true,
          status: data?.status
        }

      }
      return item;

    });
    setQuestionsList(updatedQtns);
    if (data?.quesid == lastItem) {
      setISDisabled(false);
    }

  }

  const nextPage = () => {
    pushNotificationRespData('');
    dispatch(statusbarUpdateSagaAction({ questions: true }));
    navigate(`/video/${vcipid}/location-check`)
    setIsLoading(true);

  }

  const pushNotificationRespData = (ques) => {
    const model = {
      notificationid: "1",
      vcipkey: sessionStorage.getItem('vcipkey'),
      notifymsg: ques
    }
    // this.props.PushNotificationAction(model);
    dispatch(pushNotificationSaga({ model: model, callback: pushNotificationResp }));
  }

  const pushNotificationResp = () => {

  }

  return (
    <>
      <Header />
      <QuestionsCmp
        questionsList={questionsList}
        askQuestion={askQuestion}
        handleRemorks={handleRemorks}
        submitQuestion={submitQuestion}
        nextPage={nextPage}
        isLoading={isLoading}
        isDisabled={isDisabled}
        lastItem={lastItem}
      />
    </>
  )
}

export default Questions