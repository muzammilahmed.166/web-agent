import React, { useContext, useState } from 'react';
import { useDispatch } from 'react-redux';
import AadharDetailsCheckCmp from '../../Components/VideoCallCmp/AadharDetailsCheckCmp';
import { statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import { VCIPDetailsContext } from './VideoDashboard';

const AadharDetailsCheck = () => {
  const { customerVcipDetails, navigate, vcipid } = useContext(VCIPDetailsContext);
  const [isLoading, setIsLoading] = useState(false);

  const dispatch = useDispatch();

  const nextPage = () => {
    setIsLoading(true)
    dispatch(statusbarUpdateSagaAction({ checkAadhar: true }));
    navigate(`/video/${vcipid}/pan`)
  }

  return (
    <>
      <AadharDetailsCheckCmp
        customerVcipDetails={customerVcipDetails}
        nextPage={nextPage}
        isLoading = {isLoading}
      />
    </>
  )
}

export default AadharDetailsCheck;