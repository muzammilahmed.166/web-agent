import React, { useContext, useState } from 'react'
import { useDispatch } from 'react-redux';
import PANDetailsCheckCmp from '../../Components/VideoCallCmp/PANDetailsCheckCmp';
import { statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import { VCIPDetailsContext } from './VideoDashboard';

const PANDetailsCheck = () => {
  const { customerVcipDetails, endCustomerAgentVideo } = useContext(VCIPDetailsContext);
  const [isLoading,setIsLoading] = useState(false)

  const dispatch = useDispatch();

  const nextPage = () => {
    setIsLoading(true);
    dispatch(statusbarUpdateSagaAction({ checkPAN: true }));
    endCustomerAgentVideo();
    // navigate(`/video/${vcipid}/pan`)
  }

  return (
    <>
      <PANDetailsCheckCmp
        customerVcipDetails={customerVcipDetails}
        nextPage={nextPage}
        isLoading ={isLoading}
      />
    </>
  )
}

export default PANDetailsCheck;