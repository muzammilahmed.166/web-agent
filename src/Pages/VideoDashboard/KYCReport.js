import React, { useContext, useEffect, useState } from 'react'
import toast from 'react-hot-toast';
import { useDispatch } from 'react-redux';
import KYCReportCmp from '../../Components/VideoCallCmp/KYCReportCmp'
import { VCIPDetailsContext } from './VideoDashboard';
import { statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';

// import './report.css';

const KYCReport = () => {
    const { customerVcipDetails, navigate, vcipid, updateKYCReportFinalStatus } = useContext(VCIPDetailsContext);
    const [isMatchChecked, setIsMatchChecked] = useState(false);
    const [isSelected, setIsSelected] = useState('');
    const [selectedVal, setSelectedVal] = useState('');
    const [remarks, setRemarks] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    const handleRemorks = (e) => {
        const { name, value } = e.target;
        setRemarks(value)
    }
    const dispatch = useDispatch();

    const selectedStatus = (val) => {
        setIsSelected(val === '2');
        setIsMatchChecked(true)
        setSelectedVal(val);
    }

    const submitKyc = () => {
        if (!remarks && selectedVal === "3") {
            toast.error("Please enter remarks for rejected")
            return;
        }
        setIsLoading(true)
        updateKYCReportFinalStatus(selectedVal, remarks);
        dispatch(statusbarUpdateSagaAction({ 
            questions: false,
            captureFace: false,
            checkLocation: false,
            checkAadhar: false,
            checkPAN: false,
            instructions: false,
            // signature: false,
            isCallEnded: false,
         }))

    }

    return (
        <>
            <KYCReportCmp
                customerVcipDetails={customerVcipDetails}
                isMatchChecked={isMatchChecked}
                isSelected={isSelected}
                selectedStatus={selectedStatus}
                submitKyc={submitKyc}
                handleRemorks={handleRemorks}
                isLoading = {isLoading}
            />
        </>
    )
}

export default KYCReport