import React, { useContext } from 'react'
import { useDispatch } from 'react-redux';
import { statusbarUpdateSagaAction } from '../../Store/SagaActions/CommonSagaActions';
import { VCIPDetailsContext } from './VideoDashboard';

const CheckLocation = () => {
  const { customerVcipDetails, navigate, vcipid } = useContext(VCIPDetailsContext);

  const dispatch = useDispatch();

  const nextPage = () => {
    dispatch(statusbarUpdateSagaAction({ checkLocation: true }));
    navigate(`/video/${vcipid}/capture-face`)
  }

  return (
    <>
      <article className='prt-1'>
        <h5>Check Location</h5>
        <hr className="hr-horizontal" />
        <div className="check-location">
            <div className="mapouter">
              <div className="gmap_canvas w-100 border">
                <iframe width={700} height={200} id="gmap_canvas" src="https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&t=&z=13&ie=UTF8&iwloc=&output=embed" 
                frameBorder={0} scrolling="no" marginHeight={0} marginWidth={0} />
                </div>
            </div>
          <div className="map-details">
            <div className="row sub-map-details">
              <div className="col-6">
                <div className='d-flex lctn-data'>
                  <span>Address:</span>
                  <h3 className='m-name'> {customerVcipDetails?.customerdetails?.per_address}</h3>
                </div>
                <div className='d-flex lctn-data'>
                  <span>Location:</span>
                  <h3 className='m-name'> {customerVcipDetails?.customerdetails?.geo_location}</h3>
                </div>
                <div className='d-flex lctn-data'>
                  <span>Lat & Log:</span>
                  <h3 className='m-name'> {customerVcipDetails?.customerdetails?.location}</h3>
                </div>
              </div>
              <div className="col-6">
                <div className='d-flex lctn-data'>
                  <span>IP Address:</span>
                  <h3 className='m-name'> {customerVcipDetails?.customerdetails?.ipaddress}</h3>
                </div>
                {/* <div className='d-flex lctn-data'>
                  <span>Pincode:</span>
                  <h3 className='m-name'> Banglore</h3>
                </div> */}
              </div>
              {/* <div className="col-4">
                <div className='d-flex lctn-data'>
                  <span>Country:</span>
                  <h3 className='m-name'> Banglore north</h3>
                </div>
                <div className='d-flex lctn-data'>
                  <span>IP Address:</span>
                  <h3 className='m-name'> {customerVcipDetails?.customerdetails?.ipaddress}</h3>
                </div>
              </div> */}
            </div>
            <hr className="hr-horizontal" />
            <div className="map-proxy">
              <img src="../../images/Vector-solid-right.svg" alt="" />
              <h4>SAFE IP ADDRESS</h4>
              <span>-</span>
              <h5> Proxy/VPN not detected I India</h5>
            </div>
          </div>
          {/* <div className="frm-grp">
            <input
              type="text"
              name=""
              className='frm-grp-inp'
              placeholder='Add remarks (optional)'
            />
          </div> */}
        </div>

      </article>
      <article className='prt-2'>
        <button className="btn" onClick={nextPage}>Next</button>
      </article>
    </>
  )
}

export default CheckLocation