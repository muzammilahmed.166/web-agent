import React, { useState } from 'react';
import toast from 'react-hot-toast';
import { statusbarUpdateSagaAction } from '../../../../Store/SagaActions/CommonSagaActions';
import { useDispatch } from 'react-redux';



const IssueModal = (props) => {

    const [issueList, setIssueList] = useState({});
    const [issuetext, setIssuetext] = useState("");
    const [remarks, setRemarks] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const dispatch = useDispatch();


    const handleChange = (e) => {
        const { name, value } = e.target;
        let copyIssueList = { ...issueList };
        if (name in copyIssueList) {
            delete copyIssueList[name];
        } else {
            copyIssueList[name] = value;
        }
        let convertToStr = Object.values(copyIssueList)?.join(',');
        setIssueList(copyIssueList);
        // setRemarks(convertToStr);
        setIssuetext(convertToStr)
    }

    const handleRemorks = (e) => {
        const { name, value } = e.target;
        setRemarks(value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (remarks) {
            let finalVal = issuetext + "," + remarks;
            props?.endVcipCallWithIssue(finalVal);
        } else {
            toast.error("Please enter remarks");
        }
        setIsLoading(true);
        dispatch(statusbarUpdateSagaAction({ 
            questions: false,
            captureFace: false,
            checkLocation: false,
            checkAadhar: false,
            checkPAN: false,
            instructions: false,
            // signature: false,
            isCallEnded: false,
         }))
    }


    return (
        <>
            <div className="modal-header">
                <h5 className="modal-title m-auto" id="exampleModalLabel">Facing an issue during the call?</h5>
                <img src="../../images/Vector_close.svg" className="close" onClick={props?.closeModal} alt="" />
            </div>

            <div className="modal-body questions-modal">
                <p className="checkbox-option mt-3 mb-3">Kindly select one ofthe below option to notify the user or end the call.
                </p>
                <div className="row">
                    <div className="col-md-6">
                        <ul className="checkbox-order ps-0">
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue1"
                                    name="issue1"
                                    defaultValue="Customer connection is weak"
                                />
                                <label htmlFor="issue1" className="connection-text"> Customer connection is weak</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue2"
                                    name="issue2"
                                    defaultValue="Customer is constantly moving"
                                />
                                <label htmlFor="issue2" className="connection-text"> Customer is constantly moving</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue3"
                                    name="issue3"
                                    defaultValue="Customer is unable to follow instructions"
                                />
                                <label htmlFor="issue3" className="connection-text"> Customer is unable to follow instructions</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue4"
                                    name="issue4"
                                    defaultValue="Unable to see the customer" />
                                <label htmlFor="issue4" className="connection-disabletext"> Unable to see the customer</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue5"
                                    name="issue5"
                                    defaultValue="Language barrier" />
                                <label htmlFor="issue5" className="connection-disabletext"> Language barrier</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue6"
                                    name="issue6"
                                    defaultValue="Customer doesn’t have pan card" />
                                <label htmlFor="issue6" className="connection-disabletext"> Customer doesn’t have pan card</label>
                            </li>
                        </ul>
                    </div>
                    <div className="col-md-6 general-information">
                        <ul className="checkbox-order ps-0">
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue7"
                                    name="issue7"
                                    defaultValue="Customer will call again/reschdule" />
                                <label htmlFor="issue7" className="connection-disabletext">Customer will call again/reschdule.</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue8"
                                    name="issue8"
                                    defaultValue="Unable to hear the customer" />
                                <label htmlFor="issue8" className="connection-disabletext"> Unable to hear the customer
                                </label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue9"
                                    name="issue9"
                                    defaultValue="Customer behaviour issues" />
                                <label htmlFor="issue9" className="connection-disabletext">Customer behaviour issues</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue10"
                                    name="issue10"
                                    defaultValue="Customer is fraudulent" />
                                <label htmlFor="issue10" className="connection-disabletext"> Customer is fraudulent</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue11"
                                    name="issue11"
                                    defaultValue="PAN card is not valid or tampered" />
                                <label htmlFor="issue11" className="connection-disabletext">PAN card is not valid or tampered</label>
                            </li>
                            <li>
                                <input
                                    type="checkbox"
                                    className="checkbox-modal"
                                    onChange={handleChange}
                                    id="issue12"
                                    name="issue12"
                                    defaultValue="Other" />
                                <label htmlFor="issue12" className="connection-disabletext">Other</label>
                            </li>
                        </ul>
                    </div>
                    {/* <div className="form-group p-3 group-form">
                        <textarea
                            className="form-control add-remark w-100"
                            maxLength={400}
                            rows={4}
                            disabled
                            readOnly
                            placeholder="Selected issues"
                            defaultValue={issuetext}></textarea>
                    </div> */}
                    <div className="form-group p-3 group-form">
                        {/* <input type="text" className="form-control add-remark" placeholder="Add remarks (optional)" defaultValue /> */}
                        <textarea
                            className="form-control add-remark w-100"
                            onChange={handleRemorks}
                            maxLength={100}
                            placeholder="Add remarks (optional)"
                            required
                            defaultValue={remarks}></textarea>
                    </div>
                    <div>
                        <div className='text-center'>
                            {/* <button type="button" className="modal-button btn btn-white mx-2" style={{ width: 100 }} data-dismiss="modal">Notify</button> */}
                            <button
                                // onClick={handleSubmit}
                                type="button"
                                className="modal-button btn mx-2 disabled"
                                style={{ width: 100 }}
                                data-dismiss="modal"
                                disabled>
                                    Notify
                            </button>
                            <button
                                onClick={handleSubmit}
                                type="button"
                                className="modal-button btn mx-2"
                                style={{ width: 100 }}
                                data-dismiss="modal">
                                End call{isLoading ? <span className ="spinner"></span>:null}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default IssueModal