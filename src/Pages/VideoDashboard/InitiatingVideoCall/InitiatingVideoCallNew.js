import { OpenVidu } from 'openvidu-browser';
import React, { useContext, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import InitiatingVideoCallCmp from '../../../Components/VideoCallCmp/InitiatingVideoCallCmp'
import RouteNames from '../../../Constants/RouteNames';
import {
  createVideoSessionSagaAction,
  endVideoCallSagaAction,
  updateVcipStatusSagaAction
} from '../../../Store/SagaActions/VideoSagaActions';
import OpenViduVideoComponent from './OvVideo';
// import AppFooter from '../../Common/AppFooter';
import VideoSession from './VideoSession';
import './video.css';
import { actionNoficationListSaga, pushNotificationSaga } from '../../../Store/SagaActions/CommonSagaActions';
import { VCIPDetailsContext } from '../VideoDashboard';
import Axios from 'axios';
import base64 from 'base-64';
import AES256 from 'aes-everywhere';

let KEY = 'f379e0b661ae4650b19169e4d93665dc';

const aesEncrypt = (data) => {
  var passphrase = KEY;
  let val1 = passphrase.substr(0, 4);
  let val2 = passphrase.substr(passphrase.length, 4);
  let updatedValue = val1 + passphrase + val2;
  const finalvalue = base64.encode(updatedValue).substr(0, 32);
  const encrypted = AES256.encrypt(JSON.stringify(data), finalvalue);
  return encrypted;
}

const aesDecrypt = (data) => {
  var passphrase = KEY;
  let val1 = passphrase.substr(0, 4);
  let val2 = passphrase.substr(passphrase.length, 4);
  let updatedValue = val1 + passphrase + val2;
  const finalvalue = base64?.encode(updatedValue).substr(0, 32);
  const decrypted = AES256?.decrypt(data, finalvalue);
  return decrypted;
}

const InitiatingVideoCall = () => {
  const [session, setSession] = useState(undefined);
  const [session2, setSession2] = useState(undefined)
  const [subscribers, setSubscribers] = useState([]);
  const [mainStreamManager, setMainStreamManager] = useState(undefined);
  const [publisher, setPublisher] = useState(undefined);
  // const [notificationIntervalId, setNotificationIntervalId] = useState('');
  const [notificationList, setNotificationList] = useState({});
  const [displayQtn, setDisplayQtn] = useState({});
  // const [OV, setOV] = useState(undefined);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const session2 = useSelector(state => state.VcipReducer.session2)
  const videoTokenData = useSelector(state => state.VcipReducer.videoTokenData)

  const params = useParams();

  const { customerVcipDetails } = useContext(VCIPDetailsContext);

  var OV;
  var notificationIntervalId;

  useEffect(() => {
    let OV = new OpenVidu();
    let mySession = OV.initSession();
    setSession(mySession);
    // setOV(OV);
    joinVideoSession();
    return () => {
      endVideoCall();
      clearNotificationInterval();
      leaveCallWithoutEndSession();
      // leaveSession();
      if (session2) {
        session2.disconnect();
      }
    }
  }, []);

  // useEffect(() => {
  // if (notificationList?.notificationid === '2') {
  // endVideoCall();
  // }
  // }, [notificationList])
  useEffect(() => {
    if (videoTokenData) {
      const sessionId = params.id;
      recordVideo(sessionId);
    }
  }, [videoTokenData])

  // useEffect(() => {
  //   if (session && Object.keys(session)?.length > 0) {
  //     console.log("=====>shdahag", session);
  //     // joinVideoSession();
  //   }

  // }, [session])



  const clearNotificationInterval = () => {
    console.log("notificationIntervalId-->", notificationIntervalId);
    if (notificationIntervalId) {
      clearInterval(notificationIntervalId)
    }
  }

  const joinVideoSession = () => {
    OV = new OpenVidu();
    var session = OV.initSession();

    sessionStorage.setItem("mySession", session);
    // var mySession = this.state.session;
    session.on('streamCreated', (event) => {
      var subscriber = session.subscribe(event.stream, undefined);
      // console.log("==================event.stream", subscriber);
      if (event.stream.typeOfVideo !== "SCREEN") {
        sessionStorage.setItem("connectionId", event.stream.connection.connectionId);
      }
      var subscribers$ = subscribers;
      subscribers$?.push(subscriber);
      sessionStorage.setItem("subscribers", Object.keys(subscribers$))
      setSubscribers(subscribers$);
    });
    session.on('streamDestroyed', (event) => {
      session.connect(null, (err) => {
        console.log("============>", err);
      })
      deleteSubscriber(event.stream.streamManager);
    });
    // console.log("###############################OV", OV);
    const name = params.id;
    // const name = "22190";
    const model = {
      name: name,
      sessionId: name,
      OV: OV,
      session: session,
      myUserName: name
    }
    dispatch(createVideoSessionSagaAction({
      model: model,
      setMainStreamManager,
      setPublisher,
      setSession2,
      callback: getCreateSessionData,
      callback2: getMySessionData
    }));
  }

  const getMySessionData = (data) => {
    console.log("data?.session2===========>", data);
    setSession2(data?.session2);
  }

  const getCreateSessionData = (data) => {
    // console.log("===========comp", data);
    setMainStreamManager(data.mainStreamManager)
    setPublisher(data.publisher);
    // notificationIntervalId = setInterval(() => {
    //   // console.log("nfyId", notificationIntervalId);
    //   // if (!notificationIntervalId) {
    //   // setNotificationIntervalId(nfyId)
    //   // }
    //   dispatch(actionNoficationListSaga({ callback: getNoficationData }));
    // }, 1000);
  }

  const recordVideo = (sessionId) => {
    var data = {
      session: sessionId
    };
    let body = {
      "data": aesEncrypt(data)
    }
    Axios.post('https://vcip.syntizen.com:3002/vkycrestservices/tokens', body, {
      headers: {
        'Content-Type': 'application/json',
      },
    }).then((resp) => {
      // const mySession = $this.state.session;
      var response = JSON.parse(aesDecrypt(resp.data.data));
      sessionStorage.setItem("session", response.session)
      let OV2 = new OpenVidu();
      let mySession2 = OV2.initSession();
      mySession2.connect(response.token, "screen")
        .then(() => {
          let publisher2 = OV2.initPublisher(undefined, {
            audioSource: undefined,
            videoSource: 'screen',
            publishAudio: true,
            publishVideo: true,
            // resolution: '640x480',
            frameRate: 30,
            insertMode: 'APPEND',
            mirror: false,
          });
          mySession2.publish(publisher2);
          setSession2(mySession2);
        })
        .catch((error) => {
          console.log('There was an error connecting to the session:', error.code, error.message);
        });
    }).catch((error) => {
      console.log(error);
    })
  }


  const getNoficationData = (data) => {
    setNotificationList(data);
    // if (data?.notificationid === '1') {
    // } else if (data?.notificationid === '2') {
    // }
  }

  // DELETE VIDEO CALL SUBSCRIBER
  const deleteSubscriber = (streamManager) => {
    let subscribersData = subscribers;
    let index = subscribersData?.indexOf(streamManager, 0);
    if (index > -1) {
      subscribersData?.splice(index, 1);
      setSubscribers(subscribersData);
    }
  }


  // END CALL WITHOUT CLICKING END BUTTON
  const leaveCallWithoutEndSession = () => {
    // const mySession = this.state.session;
    if (session) {
      session.disconnect();
    }
    // OV = null;

    // setOV(undefined);
    setSubscribers([]);
    setMainStreamManager(undefined);
    setSession(undefined);
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }

  // END VIDEO CALL
  const leaveSession = () => {
    // clearInterval(this.state.intervalId1);
    clearNotificationInterval();
    // const mySession = this.state.session;
    if (session) {
      session.disconnect();
      // toast.error("Customer Disconnected");
    }
    // OV = null;
    // setOV(undefined);
    setSubscribers([]);
    setMainStreamManager(undefined);
    setSession(undefined);
    const model = {
      notificationid: "2",
      vcipkey: sessionStorage.getItem('vcipkey'),
      notifymsg: "Call End"
    }
    dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
    dispatch(pushNotificationSaga({ model: model, callback: pushNotificationRespData }));
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }

  const getEndCallRespData = (data) => {
    // const model = {
    //   vcipkey: sessionStorage.getItem('vcipkey'),
    //   vcipstatus: "3", // 2 - Approved, 3- Rejected
    //   agentstatus: "3", // 1 - Approved, 2 - Rejected  3-  issued
    //   remarks: ""
    // }
    // dispatch(updateVcipStatusSagaAction({ model: model, callback: updateVcipStatusRes }));
  }

  const updateVcipStatusRes = () => {
    console.log("===========>");
    navigate('/dashboard/live', { replace: true });
  }

  const pushNotificationRespData = (data) => {
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
    endVideoCall();
    stopScreenRecord();
    console.log("session2===comp", session2);
  }

  const stopScreenRecord = () => {
    console.log("session2session2session2session2session2", session2);
    if (session2) {
      session2.disconnect();
    }
  }


  // END VIDEO CALL
  const leaveByCustomerSession = () => {
    clearInterval(this.state.intervalId1);
    // const mySession = this.state.session;
    if (session) {
      session.disconnect();
      // toast.error("Customer Disconnected");
    }
    // this.OV = null;
    // setOV(undefined);
    this.setState({
      session: undefined,
      subscribers: [],
      mySessionId: '',
      myUserName: '',
      mainStreamManager: undefined,
      publisher: undefined
    });
    // this.props.EndVideoAction();
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
    // $('#endCallModalByAgent').modal('show');
    // setTimeout(() => {
    //     $('#endCallModalByAgent').modal('hide');
    // }, 3000);
    // this.finishCheck();
  }



  // END VIDEO CALL
  const endVideoCall = () => {
    if (session) {
      session.disconnect();
    }
    sessionStorage.removeItem("session");
    sessionStorage.removeItem("connectionId");
    // sessionStorage.removeItem("videoconfsessionid");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
    // setTimeout(() => {
    // navigate('/dashboard/live', { replace: true });
    // }, 1000);
  }

  const nextPage = () => {
    navigate(RouteNames.KYC_PROCESS_COMPLETED);
  }

  console.log("op->->subscribers", subscribers, "op->->mainStreamManager", mainStreamManager, "op->->session", session);

  return (
    <>
      {!session
        ? <InitiatingVideoCallCmp />
        : <>
          {subscribers && subscribers.length !== 0 ? null
            : <p className="text-center text-danger position-absolute">
              not Available
            </p>
          }
          <canvas id="canvas" style={{ objectFit: 'fill', position: "absolute", width: "100%", height: "100%", display: "none" }}></canvas>


          <div className="row m-0 flex-1">
            <div className="col-6 ps-0">
              <div className="vd-bx">
                <div className="vd-bx-agnt">
                  <span className='vd-name'>Agent</span>
                  {/* <div className="myvideo h-100 position-relative"> */}
                  {/* {mainStreamManager ? (
                    // <div id="main-video">
                    <OpenViduVideoComponent streamManager={mainStreamManager} />
                    // </div>
                  ) : null} */}
                  {/* </div> */}

                  {/* {subscribers?.map((sub, i) => {
                    if (sub.stream.typeOfVideo !== "SCREEN") {
                      return <div key={i} className="stream-container othervideo">
                        <VideoSession streamManager={sub} />
                      </div>
                    }
                  })} */}

                  {mainStreamManager ? (
                    <div id="cusomer" className='h-100'>
                      <OpenViduVideoComponent streamManager={mainStreamManager} />
                    </div>
                  ) : null}
                </div>
                <ul className="vd-bx-list-icons">
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-mic.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Customer details</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li><span>Name:</span> {customerVcipDetails?.customerdetails?.name}</li>
                        <li><span>Father name:</span> {customerVcipDetails?.customerdetails?.fname} </li>
                        <li><span>Date of birth:</span> {customerVcipDetails?.customerdetails?.dob}</li>
                        <li><span>Gender:</span> {customerVcipDetails?.customerdetails?.gender}</li>
                        <li><span>Email id:</span> {customerVcipDetails?.customerdetails?.email}</li>
                        <li><span>Mobile number:</span> {customerVcipDetails?.customerdetails?.mobile}</li>
                        <li><span>Current location:</span> {customerVcipDetails?.customerdetails?.curr_address}</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-location.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Customer location</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li><span>Lat & Long:</span> {customerVcipDetails?.customerdetails?.location}</li>
                        <li><span>Location:</span> {customerVcipDetails?.customerdetails?.geo_location}</li>
                        <li><span>Address</span> {customerVcipDetails?.customerdetails?.per_address}</li>
                        <li><span>IP Address</span> {customerVcipDetails?.customerdetails?.ipaddress}</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-network.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Agent speed network</h6>
                      <hr />
                      <ul className="vd-bx-info-lst d-flex justify-content-around align-items-center">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.nw_incoming}<br />  <span>Incoming</span></li>
                        <li className='text-center'>{customerVcipDetails?.customerdetails?.nw_outgoing} <br /> <span>Outgoing</span>  </li>
                      </ul>
                      <h6 className='vd-bx-info-ttl'>Customer speed network</h6>
                      <hr />
                      <ul className="vd-bx-info-lst d-flex justify-content-around align-items-center mt-2">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.nw_incoming}<br />  <span>Incoming</span></li>
                        <li className='text-center'>{customerVcipDetails?.customerdetails?.nw_outgoing} <br /> <span>Outgoing</span>  </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-design.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Agent Video resolution</h6>
                      <hr />
                      <ul className="vd-bx-info-lst mt-2">
                        <li className='text-center'> {window.screen.width} x {window.screen.height} <br /><span>Pixels</span> </li>
                      </ul>
                      <h6 className='vd-bx-info-ttl'>Customer video resolution</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.videoresolution} <br /> <span>Pixels</span></li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-error.svg" alt="" />
                    </button>
                  </li>
                </ul>
              </div>
              <button className='btn' onClick={leaveSession}>End Call</button>
            </div>
            <div className="col-6">
              <div className="vd-bx h-100">
                <div className="vd-bx-agnt h-100">
                  <span className='vd-name'>Customer</span>

                  {/* {mainStreamManager ? (
                    <div id="cusomer" className='h-100'>
                      <OpenViduVideoComponent streamManager={mainStreamManager} />
                    </div>
                  ) : null} */}

                  {subscribers?.map((sub, i) => {
                    if (sub.stream.typeOfVideo !== "SCREEN") {
                      // return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                      return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                        <VideoSession streamManager={sub} />
                      </div>
                    }
                  })}
                </div>
              </div>
            </div>
          </div>


          {notificationList && Object.values(notificationList)?.length > 0
            ? (notificationList?.notificationid === '1' && notificationList?.notifications ? <div className='display-qtn'>
              <span>{notificationList?.notifications}</span>
            </div> : (null))
            : (null)}

          {/* <VideoSession subscribers={subscribers} /> */}
        </>
      }
    </>
  )
}

export default InitiatingVideoCall;