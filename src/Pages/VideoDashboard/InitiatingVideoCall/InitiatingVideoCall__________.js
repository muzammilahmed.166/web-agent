import { OpenVidu } from 'openvidu-browser';
import React, { useContext, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import InitiatingVideoCallCmp from '../../../Components/VideoCallCmp/InitiatingVideoCallCmp'
import RouteNames from '../../../Constants/RouteNames';
import { createVideoSessionSagaAction, endVideoCallSagaAction, updateVcipStatusSagaAction } from '../../../Store/SagaActions/VideoSagaActions';
import OpenViduVideoComponent from './OvVideo';
// import AppFooter from '../../Common/AppFooter';
import VideoSession from './VideoSession';
import './video.css';
import { actionNoficationListSaga, pushNotificationSaga } from '../../../Store/SagaActions/CommonSagaActions';
import { VCIPDetailsContext } from '../VideoDashboard';

const InitiatingVideoCall = () => {
  const [session, setSession] = useState(undefined);
  const [session2, setSession2] = useState(undefined)
  const [subscribers, setSubscribers] = useState([]);
  const [mainStreamManager, setMainStreamManager] = useState(undefined);
  const [publisher, setPublisher] = useState(undefined);
  // const [notificationIntervalId, setNotificationIntervalId] = useState('');
  const [notificationList, setNotificationList] = useState({});
  const [displayQtn, setDisplayQtn] = useState({});
  // const [OV, setOV] = useState(undefined);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const session2 = useSelector(state => state.VcipReducer.session2)
  const videoTokenData = useSelector(state => state.VcipReducer.videoTokenData);
  const isCallEnded = useSelector(state => state.HomeReducer.isCallEnded);

  const params = useParams();

  const { customerVcipDetails, stop } = useContext(VCIPDetailsContext);

  var OV;
  var notificationIntervalId;

  useEffect(() => {
    let OV = new OpenVidu();
    let mySession = OV.initSession();
    setSession(mySession);
    joinVideoSession();
    return () => {
      if (session) {
        session.disconnect();
      }
      // leaveCallWithoutEndSession();
    }
  }, []);

  useEffect(() => {
    console.log(session, isCallEnded, "isCallEndedisCallEndedisCallEndedisCallEndedisCallEndedisCallEndedisCallEndedisCallEndedisCallEnded");
    if (isCallEnded) {
      console.log("-----yesssssssssss", session);      
      if (session) {
        endVideoCall()
      }
    }

  }, [isCallEnded])

  const clearNotificationInterval = () => {
    console.log("notificationIntervalId-->", notificationIntervalId);
    if (notificationIntervalId) {
      clearInterval(notificationIntervalId)
    }
  }

  const joinVideoSession = () => {
    OV = new OpenVidu();
    var session = OV.initSession();
    sessionStorage.setItem("mySession", session);
    session.on('streamCreated', (event) => {
      var subscriber = session.subscribe(event.stream, undefined);
      if (event.stream.typeOfVideo !== "SCREEN") {
        sessionStorage.setItem("connectionId", event.stream.connection.connectionId);
      }
      var subscribers$ = subscribers;
      subscribers$?.push(subscriber);
      sessionStorage.setItem("subscribers", Object.keys(subscribers$))
      setSubscribers(subscribers$);
    });
    session.on('streamDestroyed', (event) => {
      session.connect(null, (err) => {
        console.log("============>", err);
      })
      deleteSubscriber(event.stream.streamManager);
    });
    const name = params.id;
    const model = {
      name: name,
      sessionId: name,
      OV: OV,
      session: session,
      myUserName: name
    }
    dispatch(createVideoSessionSagaAction({
      model: model,
      setMainStreamManager,
      setPublisher,
      setSession2,
      callback: getCreateSessionData,
      callback2: getMySessionData
    }));
  }

  const getCreateSessionData = (data) => {
    const sessionId = params.id;
    setMainStreamManager(data.mainStreamManager)
    setPublisher(data.publisher);
  }

  const getMySessionData = (data) => {
    console.log("data?.session2===========>", data);
    setSession2(data?.session2)
  }

  const getNoficationData = (data) => {
    setNotificationList(data);
  }

  // DELETE VIDEO CALL SUBSCRIBER
  const deleteSubscriber = (streamManager) => {
    let subscribersData = subscribers;
    let index = subscribersData?.indexOf(streamManager, 0);
    if (index > -1) {
      subscribersData?.splice(index, 1);
      setSubscribers(subscribersData);
    }
  }


  // END CALL WITHOUT CLICKING END BUTTON
  const leaveCallWithoutEndSession = () => {
    const model = {
      notificationid: "2",
      vcipkey: sessionStorage.getItem('vcipkey'),
      notifymsg: "Call End"
    }
    // dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
    dispatch(pushNotificationSaga({ model: model, callback: pushNotificationRespData }));
  }

  // END VIDEO CALL
  const leaveSession = () => {
    if (session) {
      session.disconnect();
    }
    const model = {
      notificationid: "2",
      vcipkey: sessionStorage.getItem('vcipkey'),
      notifymsg: "Call End"
    }
    dispatch(endVideoCallSagaAction({ callback: getEndCallRespData }));
    dispatch(pushNotificationSaga({ model: model, callback: pushNotificationRespData }));
    setSubscribers([]);
    setMainStreamManager(undefined);
    setSession(undefined);
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }

  const getEndCallRespData = (data) => {
    const model = {
      vcipkey: sessionStorage.getItem('vcipkey'),
      vcipstatus: "3", // 2 - Approved, 3- Rejected
      agentstatus: "3", // 1 - Approved, 2 - Rejected  3-  issued
      remarks: ""
    }
    stop();
    dispatch(updateVcipStatusSagaAction({ model: model, callback: updateVcipStatusRes }));
  }

  const updateVcipStatusRes = () => {
    navigate('/dashboard/live', { replace: true });
  }

  const pushNotificationRespData = (data) => {
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
    endVideoCall();
  }

  const stopScreenRecord = () => {
    if (session2) {
      session2.disconnect();
    }
  }


  // END VIDEO CALL
  const leaveByCustomerSession = () => {
    clearInterval(this.state.intervalId1);
    const mySession = this.state.session;
    if (mySession) {
      mySession.disconnect();
    }
    this.setState({
      session: undefined,
      subscribers: [],
      mySessionId: '',
      myUserName: '',
      mainStreamManager: undefined,
      publisher: undefined
    });
    this.props.EndVideoAction();
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }



  // END VIDEO CALL
  const endVideoCall = () => {
    if (session) {
      session.disconnect();
    }
    sessionStorage.removeItem("session");
    sessionStorage.removeItem("connectionId");
    sessionStorage.removeItem("publisher");
    sessionStorage.removeItem("session");
  }

  const nextPage = () => {
    navigate(RouteNames.KYC_PROCESS_COMPLETED);
  }

  return (
    <>
      {!session
        ? <InitiatingVideoCallCmp />
        : <>
          {subscribers && subscribers.length !== 0 ? null
            : <p className="text-center text-danger position-absolute">
              not Available
            </p>
          }
          <canvas id="canvas" style={{ objectFit: 'fill', position: "absolute", width: "100%", height: "100%", display: "none" }}></canvas>


          <div className="row m-0 flex-1">
            <div className="col-6 ps-0">
              <div className="vd-bx">
                <div className="vd-bx-agnt">
                  <span className='vd-name'>Agent</span>
                  {/* <div className="myvideo h-100 position-relative"> */}
                  {/* {mainStreamManager ? (
                    // <div id="main-video">
                    <OpenViduVideoComponent streamManager={mainStreamManager} />
                    // </div>
                  ) : null} */}
                  {/* </div> */}

                  {/* {subscribers?.map((sub, i) => {
                    if (sub.stream.typeOfVideo !== "SCREEN") {
                      return <div key={i} className="stream-container othervideo">
                        <VideoSession streamManager={sub} />
                      </div>
                    }
                  })} */}

                  {mainStreamManager ? (
                    <div id="cusomer" className='h-100'>
                      <OpenViduVideoComponent streamManager={mainStreamManager} />
                    </div>
                  ) : null}
                </div>
                <ul className="vd-bx-list-icons">
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/icon-user.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Customer details</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li><span>Name:</span> {customerVcipDetails?.customerdetails?.name ? customerVcipDetails?.customerdetails?.name : 'N/A'}</li>
                        <li><span>Father name:</span> {customerVcipDetails?.customerdetails?.fname ? customerVcipDetails?.customerdetails?.fname : 'N/A'} </li>
                        <li><span>Date of birth:</span> {customerVcipDetails?.customerdetails?.dob ? customerVcipDetails?.customerdetails?.dob : 'N/A'}</li>
                        <li><span>Gender:</span> {customerVcipDetails?.customerdetails?.gender ? customerVcipDetails?.customerdetails?.gender : 'N/A'}</li>
                        <li><span>Email id:</span> {customerVcipDetails?.customerdetails?.email ? customerVcipDetails?.customerdetails?.email : 'N/A'}</li>
                        <li><span>Mobile number:</span> {customerVcipDetails?.customerdetails?.mobile ? customerVcipDetails?.customerdetails?.mobile : 'N/A'}</li>
                        <li><span>Current location:</span> {customerVcipDetails?.customerdetails?.curr_address ? customerVcipDetails?.customerdetails?.curr_address : 'N/A'}</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-location.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Customer location</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li><span>Lat & Long:</span> {customerVcipDetails?.customerdetails?.location ? customerVcipDetails?.customerdetails?.location : 'N/A'}</li>
                        <li><span>Location:</span> {customerVcipDetails?.customerdetails?.geo_location ? customerVcipDetails?.customerdetails?.geo_location : 'N/A'}</li>
                        <li><span>Address</span> {customerVcipDetails?.customerdetails?.per_address ? customerVcipDetails?.customerdetails?.per_address : 'N/A'}</li>
                        <li><span>IP Address</span> {customerVcipDetails?.customerdetails?.ipaddress ? customerVcipDetails?.customerdetails?.ipaddress : 'N/A'}</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-network.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Agent speed network</h6>
                      <hr />
                      <ul className="vd-bx-info-lst d-flex justify-content-around align-items-center">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.nw_incoming ? customerVcipDetails?.customerdetails?.nw_incoming : 'N/A'}<br />  <span>Incoming</span></li>
                        <li className='text-center'>{customerVcipDetails?.customerdetails?.nw_outgoing ? customerVcipDetails?.customerdetails?.nw_outgoing : 'N/A'} <br /> <span>Outgoing</span>  </li>
                      </ul>
                      <h6 className='vd-bx-info-ttl'>Customer speed network</h6>
                      <hr />
                      <ul className="vd-bx-info-lst d-flex justify-content-around align-items-center mt-2">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.nw_incoming ? customerVcipDetails?.customerdetails?.nw_incoming : 'N/A'}<br />  <span>Incoming</span></li>
                        <li className='text-center'>{customerVcipDetails?.customerdetails?.nw_outgoing ? customerVcipDetails?.customerdetails?.nw_outgoing : 'N/A'} <br /> <span>Outgoing</span>  </li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-design.svg" alt="" />
                    </button>
                    <div className="vd-bx-info">
                      <h6 className='vd-bx-info-ttl'>Agent Video resolution</h6>
                      <hr />
                      <ul className="vd-bx-info-lst mt-2">
                        <li className='text-center'> {window.screen.width} x {window.screen.height} <br /><span>Pixels</span> </li>
                      </ul>
                      <h6 className='vd-bx-info-ttl'>Customer video resolution</h6>
                      <hr />
                      <ul className="vd-bx-info-lst">
                        <li className='text-center'> {customerVcipDetails?.customerdetails?.videoresolution ? customerVcipDetails?.customerdetails?.videoresolution : 'N/A'} <br /> <span>Pixels</span></li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <button className="vd-bx-list-icon-btn">
                      <img src="../../images/Vector-error.svg" alt="" />
                    </button>
                  </li>
                </ul>
              </div>
              <button className='btn' onClick={leaveSession}>End Call</button>
            </div>
            <div className="col-6">
              <div className="vd-bx h-100">
                <div className="vd-bx-agnt h-100">
                  <span className='vd-name'>Customer</span>

                  {/* {mainStreamManager ? (
                    <div id="cusomer" className='h-100'>
                      <OpenViduVideoComponent streamManager={mainStreamManager} />
                    </div>
                  ) : null} */}

                  {subscribers?.map((sub, i) => {
                    if (sub.stream.typeOfVideo !== "SCREEN") {
                      // return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                      return <div key={i} className="stream-container othervideo cstmr-vd-call h-100">
                        <VideoSession streamManager={sub} />
                      </div>
                    }
                  })}
                </div>
              </div>
            </div>
          </div>


          {notificationList && Object.values(notificationList)?.length > 0
            ? (notificationList?.notificationid === '1' && notificationList?.notifications ? <div className='display-qtn'>
              <span>{notificationList?.notifications}</span>
            </div> : (null))
            : (null)}

          {/* <VideoSession subscribers={subscribers} /> */}
        </>
      }
    </>
  )
}

export default InitiatingVideoCall;