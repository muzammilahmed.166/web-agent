import React from 'react'

const LanguageModal = (props) => {
  return (
    <>
      <div className="modal-header">
        <h5 className="modal-title m-auto" id="exampleModalLabel"> Language information </h5>
        {/* <!-- <button type="button" className="btn-close close" data-bs-dismiss="modal" aria-label="Close"></button> --> */}
        {/* <img src="../../images/Vector_close.svg" className="close" alt=""> */}
      </div>
      <div className="modal-body language-select">
        Customer has selected <b className="text-dark">‘English’</b> as a preferred languagefor on screen instructions
      </div>
      <div className="d-flex justify-content-center align-items-center my-3">
      <button type="button" className="modal-button-lng" data-dismiss="modal" onClick={props?.closeModal}>Ok</button>
      </div>
    </>

  )
}

export default LanguageModal