import React from 'react';

const AadhaarNumberVerifyModal = (props) => {
    return (
        <>
            <div className="modal-header align-items-center">
                <h5 className="modal-title text-center w-100" id="exampleModalLabel" style={{ visibility: 'hidden' }}>Aadhaar
                </h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={props.closeModal}>
                    <img src="../images/icon-close.svg" alt="close" />
                </button>
            </div>
            <div className="modal-body">
                <div className="text-center mb-3">
                    <img src="../images/icon-lock.svg" alt="lock" />
                </div>
                <h4 className="title text-center">Verification Code</h4>
                <p className="txt text-center">
                    Please enter the verification code we
                    sent to your phone number
                </p>
                <div className="secret-codes">
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                </div>
                <div className="my-5">
                    <hr />
                </div>
                <h4 className="title text-center">Enter Share Code</h4>
                <p className="txt text-center">
                    Create a 6 digit code to secure your
                    offline eKYC
                </p>
                <div className="secret-codes">
                    <input type="number" name className="scretet-inp" maxLength={1} pattern="[0-9]+" required />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    <input type="number" name className="scretet-inp" maxLength={1} min={1} />
                    {/* <input type="number" name="" class="scretet-inp" maxlength="1" min="1" />
                    <input type="number" name="" class="scretet-inp" maxlength="1" min="1" /> */}
                </div>
            </div>
            <div className="modal-footer d-flex">
                {/* <button type="button" className="btn w-auto btn-white" data-dismiss="modal" onClick={props.closeModal}>Close</button> */}
                <button type="button" className="btn w-auto" proceed onClick={props.verifyOTP}>Proceed</button>
            </div>
        </>
    )
}

export default AadhaarNumberVerifyModal;