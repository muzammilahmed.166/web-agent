import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { AddCustomerSagaAction } from '../../Store/SagaActions/AuthSagaActions';
import RouteNames from '../../Constants/RouteNames';




const AddCustomerFormModel = (props) => {
    const Applicationtype = ['agent', 'customer', 'auditor'];
    const Applicationtypes = ['agent', 'customer', 'auditor'];
    const Gender = ['M', 'F',];
    const FormatType = ['one', 'two', 'three']
    const [source, setSource] = useState("");
    const [imgFileName, setImgFileName] = useState("");

    const getBase64 = file => {
        return new Promise(resolve => {
            // let fileInfo;
            let baseURL = "";
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                baseURL = reader.result;
                resolve(baseURL);
            };
        });
    };

    function handleChange(event) {
        console.log(event.target.files[0], "event.target.files")
        if (event.target.files) {
            if (event.target.files.length !== 0) {
                const file = event.target.files[0];
                // if you want to create image link use below line
                // const newUrl = URL.createObjectURL(file);
                getBase64(file).then(result => {
                    file["base64"] = result;
                    setSource(file["base64"])
                    setImgFileName(file["name"])
                    // if (file.base64.slice(23)[0] === "/") {
                    //     localStorage.setItem('base_img', file["base64"]);
                    // } else {
                    //     setSource("")

                    // }
                }).catch(err => {
                    console.log(err)
                })
            }
        }
    };

    const [values, setValues] = useState({
        customerid: "", applicationid: "", applicationtype: "", firstname: "", lastname: "", applicationtypes: "", fathername: "",
        dateofbirth: "", gender: "", mobilenumber: "", currentaddress: "", permanentaddress: "", emailAddress: "", formattype: "", firstname1: "", lastname1: "", fathername1: "", gender1: "",
        dateofbirth1: "", mobilenumber1: "", emailaddress1: "", generationdate: "", attachmentphoto: source ? source : "", currentaddress1: "", fathername2: "", emailaddress2: "", firstname2: "", lastname2: "", dateofbirth2: "", pannumber: "", scheduleanappiontment: ""
    });
    const [selectedFile, setSelectedFile] = useState(null)

    const setgetinput = name => {
        return ({ target: { value } }) => {
            setValues(oldValues => ({ ...oldValues, [name]: value }));
            // console.log(oldValues)
        }
    };
    const [getAddCustomerList, setGetAddCustomerList] = useState([]);
    const [show, setShow] = useState(false);
    const navigate = useNavigate();
    const dispatch = useDispatch();






    const getAddCustomerForm = (event) => {
        event.preventDefault();
        const model = {

            "customerid": values?.customerid,
            "account_code": "SYNTIZEN",
            "appid": values?.applicationid,
            "apptype": values.applicationtype,
            "fullname": values?.firstname,
            "fathername": values?.fathername,
            "dob": values?.dateofbirth,
            "gender": values?.gender,
            "mobile": values?.mobilenumber,
            "curr_address": values?.currentaddress,
            "per_address": values?.permanentaddress,
            "email": values?.emailAddress,
            "kycdetails": {
                "formattype": values?.formattype,
                "fullname": values?.firstname1,
                "fathername": values?.fathername1,
                "gender": values?.gender,
                "dob": values?.dateofbirth1,
                "mobile": values?.mobilenumber1,
                "email": values?.emailaddress1,
                "generationdate": values?.generationdate,
                "image": source,
                "curr_address": values?.currentaddress1
            },
            "pandetails": {
                "fullname": values?.firstname2,
                "dob": values?.dateofbirth2,
                "pannumber": values?.pannumber
            }

        }
        console.log(model, "model")
        dispatch(AddCustomerSagaAction({ model: model, callback: getAddCustomerResp }))
    }
    const getAddCustomerResp = (respData) => {
        setGetAddCustomerList(getAddCustomerList)
        props?.closeModalChild(true)
        setValues({})        // debugger;

    }



    return (
        <>
            <div className="modal-header">
                <h5 className="modal-title m-auto" id="exampleModalLabel">
                    Add Customer
                </h5>
                <button type="button" className="close" onClick={props?.closeModal} >
                    <i className="fa-solid fa-xmark"></i>
                </button>
            </div>
            <div className="modal-body add-customer-form-model pt-4">
                <form className="lgn-rt-frm"
                 onSubmit={getAddCustomerForm}
                >

                    <div className="row ">
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Customer ID*"
                                    value={values.customerid}
                                    onChange={setgetinput('customerid')}
                                    required
                                    />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control  add-customer"
                                    placeholder="Application ID*"
                                    value={values.applicationid}
                                    onChange={setgetinput('applicationid')}
                                    required
                                    />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            {/* uncomment below */}
                            <select className="form-select application-type"
                                id="inputGroupSelect01" value={values.applicationtype} onChange={setgetinput('applicationtype')} required>
                                <option selected>Application type</option>
                                {Applicationtype.map(c => <option key={c}>{c}</option>)}


                            </select>
                           
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="First Name*"
                                    value={values.firstname}
                                    onChange={setgetinput('firstname')}
                                    required
                                    />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Last Name"
                                    value={values.lastname}
                                    onChange={setgetinput('lastname')}
                                />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <select className="form-select application-type"
                                id="inputGroupSelect01" value={values.applicationtypes} onChange={setgetinput('applicationtypes')} required>
                                <option selected>Application type</option>
                                {Applicationtypes.map(c => <option key={c}>{c}</option>)}
                            </select>
                            {/* <select
                            className="form-select application-type"
                            id="inputGroupSelect01"
                        >
                            <option selected="">Application type</option>
                            <option value={1}>One</option>
                            <option value={2}>Two</option>
                            <option value={3}>Three</option>
                        </select> */}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Father Name*"
                                    value={values.fathername}
                                    onChange={setgetinput('fathername')}
                                    required
                                    />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="date"
                                    className="form-control add-date add-customer"
                                    placeholder="Date of Birth*"
                                    content="Date of Birth*"
                                    value={values.dateofbirth}
                                    onChange={setgetinput('dateofbirth')}
                                    required
                                    />

                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            {/* uncomment below */}
                            <select className="form-select application-type"
                                id="inputGroupSelect01" value={values.gender} onChange={setgetinput('gender')} required>
                                <option  selected>Gender*</option>
                                {Gender.map(c => <option key={c}>{c}</option>)}
                            </select>
                            {/* <select
                            className="form-select application-type"
                            id="inputGroupSelect01"
                            required
                        >
                            <option className="" selected="">
                                Gender*
                            </option>
                            <option value={1}>One</option>
                            <option value={2}>Two</option>
                            <option value={3}>Three</option>
                        </select> */}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Mobile Number*"
                                    value={values.mobilenumber}
                                    onChange={setgetinput('mobilenumber')}
                                    required
                                    />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Current Address*"
                                    value={values.currentaddress}
                                    onChange={setgetinput('currentaddress')}
                                    required
                                    />
                            </div>
                        </div>
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Permanent Address*"
                                    value={values.permanentaddress}
                                    onChange={setgetinput('permanentaddress')}
                                    required
                                    />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 mb-3">
                            <div className="form-group">
                                <input
                                    type="text"
                                    className="form-control add-customer"
                                    placeholder="Email Address*"
                                    value={values.emailAddress}
                                    onChange={setgetinput('emailAddress')}
                                    required
                                    />
                            </div>
                        </div>
                    </div>
                    {/* <hr />
                        <div className="text-center">
                            <button className="cus-blue-add-btn">
                            <i className="fa fa-plus" aria-hidden="true"></i> &nbsp; Add Aadhaar Details
                            </button>
                            <button className="cus-blue-add-btn">
                            <i className="fa fa-plus" aria-hidden="true"></i> &nbsp; Add PAN Details
                            </button>
                            </div> */}          

                    <div className="line mb-3 mt-2"></div>
                    <div>
                        <div>
                            <div>
                                <h6 className="pan-details">
                                    Aadhaar Details
                                    <button className="cus-blue-aadhaar-btn">Delete</button>
                                </h6>
                            </div>
                            <div className="line mb-4 mt-3"></div>

                            <div className="row ">
                                <div className="col-md-4 mb-3">
                                    {/* uncomment below */}
                                    <select className="form-select application-type"
                                        id="inputGroupSelect01" value={values.formattype} onChange={setgetinput('formattype')}>
                                        <option selected>Format type</option>
                                        {FormatType.map(c => <option key={c}>{c}</option>)}
                                    </select>
                                    {/* <select
                                    className="form-select application-type"
                                    id="inputGroupSelect01"
                                >
                                    <option selected="">Format type</option>
                                    <option value={1}>One</option>
                                    <option value={2}>Two</option>
                                    <option value={3}>Three</option>
                                </select> */}
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="First Name*"
                                            value={values.firstname1}
                                            onChange={setgetinput('firstname1')}
                                            required
                                            />
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control  add-customer"
                                            placeholder="Last Name"
                                            value={values.lastname1}
                                            onChange={setgetinput('lastname1')}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="Father Name*"
                                            value={values.fathername1}
                                            onChange={setgetinput('fathername1')}
                                            required
                                            />
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        {/* uncomment below */}
                                        <select className="form-select application-type"
                                            id="inputGroupSelect01" value={values.gender1}
                                            onChange={setgetinput('gender1')} required>
                                            <option selected>Gender*</option>
                                            {Gender.map(c => <option key={c}>{c}</option>)}
                                        </select>
                                        {/* <input
                                        type="text"
                                        className="form-control add-customer"
                                        placeholder="Gender*"
                                        value={values.gender1}
                                        onChange={setgetinput('gender1')}
                                        /> */}
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="date"
                                            className="form-control add-date add-customer"
                                            placeholder="Date of Birth*"
                                            value={values.dateofbirth1}
                                            onChange={setgetinput('dateofbirth1')}
                                            required
                                            />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="Mobile Number*"
                                            value={values.mobilenumber1}
                                            onChange={setgetinput('mobilenumber1')}
                                            required
                                            />
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="Email Address*"
                                            value={values.emailaddress1}
                                            onChange={setgetinput('emailaddress1')}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="date"
                                            className="form-control add-date add-customer"
                                            placeholder="Generation Date*"
                                            value={values.generationdate}
                                            onChange={setgetinput('generationdate')}
                                            required
                                            />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4 mb-3">
                                    <div className="form-group attach-photo file-wrapper">
                                        <label className="custom-file-upload">
                                            <input type="file" id="getFile"
                                                accept="image/*"
                                                capture="environment"
                                                placeholder="Attach photo*"
                                                className="attach-photo"
                                                style={{ display: "none" }}
                                                //   onChange={(e) => setSelectedFile(e.target.files[0])} 
                                                onChange={(e) => handleChange(e)}

                                                />
                                            Attach photo* <span>{source ? <span className="text-dark " style={{ marginLeft: "5px" }}>{imgFileName}</span> : <img src="../../images/attach.svg" />}</span>
                                        </label>
                                        {/* <img src="../../images/attach.svg" />
                                     <button
                                        className="attach"
                                        onclick="document.getElementById('getFile').click()"
                                    >
                                        <p className="Attach-text text-start">Attach photo*</p>
                                    </button>  */}
                                        {/* <label for="file-upload" className="custom-file-upload">
    <i className="fa fa-cloud-upload"></i> Upload Image
  </label>
                                    <input type="file" id="getFile"
                                    accept="image/*"
                                    capture="environment"
                                    placeholder ="Attach photo*"
                                    className = "attach-photo"
                                    style ={{display:"none"}}
                                        //   onChange={(e) => setSelectedFile(e.target.files[0])} 
                                        onChange={(e) => handleChange(e)}

                                        /> */}

                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="Current Address*"
                                            value={values.currentaddress1}
                                            onChange={setgetinput('currentaddress1')}
                                            required
                                            />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>

                    <div className="line mb-3 mt-2"></div>

                            <div>
                                <h6 className="pan-details">
                                    PAN Details<button className="cus-blue-aadhaar-btn">Delete</button>
                                </h6>
                            </div>
                    <div className="line mb-4 mt-3"></div>

                            <div className="row ">

                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="First Name*"
                                            value={values.firstname2}
                                            onChange={setgetinput('firstname2')}
                                            required
                                            />

                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control  add-customer"
                                            placeholder="Last Name"
                                            value={values.lastname2}
                                            onChange={setgetinput('lastname2')}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="Father Name*"
                                            value={values.fathername2}
                                            onChange={setgetinput('fathername2')}
                                            required
                                            />
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="date"
                                            className="form-control add-date add-customer"
                                            placeholder="Date of Birth*"
                                            value={values.dateofbirth2}
                                            onChange={setgetinput('dateofbirth2')}
                                            required
                                            />
                                    </div>
                                </div>
                                <div className="col-md-4 mb-3">
                                    <div className="form-group">
                                        <input
                                            type="text"
                                            className="form-control add-customer"
                                            placeholder="PAN number*"
                                            value={values.pannumber}
                                            onChange={setgetinput('pannumber')}
                                            required
                                            />
                                    </div>
                                </div>
                            </div>
                            <div className="line mb-4 mt-3"></div>

                            <div className="form-check text-center">
                                <input
                                    type="checkbox"
                                    id="vehicle1"
                                    name="vehicle1"
                                    value={values.scheduleanappiontment}
                                    onChange={setgetinput('scheduleanappiontment')}
                                    
                                />

                                <label htmlFor="vehicle1" className="scheduled-text ml-3">
                                    Schedule an Appointment
                                </label>
                                <br />
                                <button
                                    type="submit"
                                    className="cus-blue-btn mt-3 w-50"
                                    data-bs-toggle="modal"
                                    data-bs-target="#exampleModal"
                                // onClick = {getAddCustomerForm}
                                >
                                    Create
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </>

    )
}

export default AddCustomerFormModel;



