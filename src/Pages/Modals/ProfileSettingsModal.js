import React from 'react'

const ProfileSettingsModal = (props) => {

    { console.log(props?.profileSettings, "profile") }
    return (
        <>
            <div className="modal-header text-center">
                <h5 className="modal-title m-auto" id="exampleModalLabel">Profile settings</h5>
                <button type="button" class="close" onClick={props?.closeModal} >
                    <i class="fa-solid fa-xmark"></i>
                </button>
            </div>
            <div className="modal-body">
                <div className="row mt-4">
                    {/* <div className="col-md-6 prof-left text-center pt-4 pb-3">
                        <img src={props?.profileSettings?.pht} />
                        <h5 className="agent-name">{props?.profileSettings?.name}</h5>
                        <hr />
                        <div>
                            <h6 className="information-phone"><img src="../../images/phone.svg" /> {props?.profileSettings?.mobile}</h6>
                            <h6 className="information-phone"><img src="../../images/email.svg" /> {props?.profileSettings?.email}</h6>
                        </div>
                    </div> */}
                    <div className="col-md-6 prof-left text-center pt-4 pb-3">

                        {/* <div className='kachra d-flex aligns-items-center justify-content-center'>
                            <h6>MA</h6>
                        </div> */}
                        <div className='cir '>
                            <img src={props?.profileSettings?.pht ? "data:image/png;base64," + props?.profileSettings?.pht : 'N/A'} />
                        </div>

                        <h5 className="mt-3">{props?.profileSettings?.name ? props?.profileSettings?.name : 'N/A'}</h5>


                        <div className='line mt-3 mb-4'></div>
                        <div>
                            <h6 ><img src="../../images/phone.svg" /> {props?.profileSettings?.mobile ? props?.profileSettings?.mobile : 'N/A'}</h6>
                            <h6 ><img src="../../images/email.svg" /> {props?.profileSettings?.email ? props?.profileSettings?.email : 'N/A'}</h6>
                        </div>
                    </div>

                    <div className="col-md-6 general-information">
                        <h6 className=" information text-start">General Information</h6>
                        <hr />
                        <form action method>
                            <table>
                                <tbody>
                                    {/* <tr>
                        <td colspan="2"></td>
                      </tr> */}
                                    <tr>
                                        {/* <hr></hr> */}
                                        <td>
                                            <h6 className="first-name text-start">Manager ID:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.manager_id ? props?.profileSettings?.manager_id : "N/A"}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start">Employee ID:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.employeeid ? props?.profileSettings?.employeeid : "N/A"}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start">Location:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.location ? props?.profileSettings?.location : "N/A"}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Region:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.region ? props?.profileSettings?.region : "N/A"}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Branch ID:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.branchid ? props?.profileSettings?.branchid : "N/A"}</h6>
                                        </td>
                                    </tr>
                                    {/* <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Work Plan:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;{props?.profileSettings?.employeeid}</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start"> Office Timings:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6 className="first-name text-start">Break Timings 1:</h6>
                                        </td>
                                        <td>
                                            <h6 className="information">&nbsp;&nbsp;</h6>
                                        </td>
                                    </tr> */}
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </>

    )
}

export default ProfileSettingsModal;