import React, { useState } from 'react'

const ChangePasswordModal = (props) => {

    const [eyeBlink, setEyeBlink] = useState({ one: false, two: false, three: false });

    const handleChangePasswordType = (value) => {
        console.log(value)
        setEyeBlink(prev => ({ ...prev, [value]: !prev[value] }))
    }

    return (
        <>
            <div>

                <div className="modal-header">
                    <h5 className="modal-title m-auto" id="exampleModalLabel">Change password</h5>
                    {/* <button type="button" class="btn-close close" data-bs-dismiss="modal" aria-label="Close"></button> */}
                    {/* <img src="../../images/Vector_close.svg" className="close" alt="" onClick={props?.closeModal} /> */}
                    <button type="button" class="close" onClick={props?.closeModal} >
                        <i class="fa-solid fa-xmark"></i>
                    </button>
                </div>
                <div className="modal-body">
                    <form className='pwd-inp'>
                        <div className="input-group cg-pswd mt-4">
                            {/* <img src='./images/lock-key-alt.svg' /> */}
                            <button type="button" className='inp-lock'>
                                {/* <img src='../images/lock-key-alt.svg' alt="lock" /> */}
                                <i class="fa-solid fa-lock-keyhole"></i>
                            </button>
                            <input
                                type={`${eyeBlink.one ? "text" : "password"}`} placeholder="Current Password" name="current" onChange={props?.handleChangePassword} />
                            <button type="button" id="one" onClick={() => handleChangePasswordType("one")}>
                                {
                                    eyeBlink.one ? <i class="fa-solid fa-eye"></i> : <i class="fa-solid fa-eye-slash"></i>
                                }
                            </button>
                        </div>

                        <div className="input-group cg-pswd mt-3">
                            <button type="button" className='inp-lock'>
                                {/* <img src='../images/lock-key-alt.svg' alt="lock" /> */}
                                <i class="fa-solid fa-lock-keyhole"></i>
                            </button>
                            <input type={`${eyeBlink.two ? "text" : "password"}`} placeholder="New Password" name="newpassword" onChange={props?.handleChangePassword} />
                            <button type="button" id="two" onClick={() => handleChangePasswordType("two")}>
                                {
                                    eyeBlink.two ? <i class="fa-solid fa-eye"></i> : <i class="fa-solid fa-eye-slash"></i>
                                }
                            </button>
                        </div>


                        <div className="input-group cg-pswd mt-3">
                            <button type="button" className='inp-lock'>
                                {/* <img src='../images/lock-key-alt.svg' alt="lock" /> */}
                                <i class="fa-solid fa-lock-keyhole"></i>
                            </button>
                            <input type={`${eyeBlink.three ? "text" : "password"}`} placeholder="Confirm Password" name="retypepassword" onChange={props?.handleChangePassword} />
                            <button type="button" id="three" onClick={() => handleChangePasswordType("three")}>
                                {
                                    eyeBlink.three ? <i class="fa-solid fa-eye"></i> : <i class="fa-solid fa-eye-slash"></i>
                                }
                            </button>
                        </div>


                        <button type="button" className="chg-btn mt-4 mb-2" onClick={props?.submitChangePassword} data-dismiss="modal">Change</button>
                    </form>
                </div>

            </div>

        </>
    )
}

export default ChangePasswordModal