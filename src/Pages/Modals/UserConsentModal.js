import React from 'react';

const UserConsentModal = (props) => {
    return (
        <>
            <div className="modal-header align-items-center">
                <h5 className="modal-title text-center w-100" id="exampleModalLabel">User Consent</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={props.closeModal}>
                    <img src="images/icon-close.svg" alt="close" />
                </button>
            </div>
            <div className="modal-body">
                <p className="txt">
                    By clicking on <b>'Agree'</b>, you hereby:
                </p>
                <ul className="video-guide">
                    <li>
                        Acknowledge the request made by
                        Syntizen technologies private limited
                        to provide personal details.
                    </li>
                    <li>
                        Provide my unconditional concent to
                        access, copy and store all information
                        therein by sharing the inofrmation.
                    </li>
                    <li>
                        Also undertake I/We are authorised
                        to do so on behalf of the requestee
                        organisation and tkae sole and
                        complete responsibilitity for the same.
                    </li>
                </ul>
            </div>
            <div className="modal-footer d-flex">
                <button type="button" className="btn w-auto btn-white" data-dismiss="modal" onClick={props.closeModal}>Disagree</button>
                <button type="button" className="btn w-auto" onClick={props.agree}>Agree</button>
            </div>
        </>
    )
}

export default UserConsentModal;