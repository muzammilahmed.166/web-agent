import React from 'react';

const NoData = (props) => {
    return (
        <article className='nodata'>
            <img src={props?.img} className='mb-3' alt='' />
            <h4 className='title'>{props?.title}</h4>
            <p className='txt'>{props?.subtitle}</p>
        </article>
    )
}

export default NoData;