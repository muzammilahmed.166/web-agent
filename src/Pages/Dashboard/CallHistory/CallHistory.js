import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';

import CallHistoryCmp from '../../../Components/DashboardCmp/CallHistoryCmp/CallHistoryCmp';
import { getCallHistorySagaAction } from '../../../Store/SagaActions/AgentDashboardSagaActions';
import NoData from '../NoData';

const CallHistory = (props) => {
    const [callHistoryList, setCallHistoryList] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        getCallHistoryList();
    }, []);

    const getCallHistoryList = () => {
        const model = {
            isall: '1',
            fdate: "2022-04-15",
            tdate: "2022-04-15"
        }
        dispatch(getCallHistorySagaAction({ model: model, callback: getLiveData }));
    }

    const getLiveData = (data) => {
        let callHistoryList = []
        if (data?.length > 0) {
            data?.map((item, i) => {
                callHistoryList.push({
                    "starttimestamp": item?.starttime, "endtimestamp": item?.endtime, "app id": item?.appid, "customername": item?.custname, "entitytype": item?.entitytype, "entityname": item?.entityname, "duration": item?.duration, "agentstatus": item?.agentstatus === '1' ? <span className="stat-bx suc">Approved</span> : item?.agentstatus === '2' ? <span className="stat-bx rej">Rejected</span> : item?.agentstatus === '3' ? <span className="stat-bx rej">Issued</span> : <td>-</td>
                })
                //         <button type="button" className="btn btn-primary w-20" data-bs-toggle="modal" data-bs-target="#viewKycReportModal">
                //             View Kyc Report
                //         </button>
                // })
            })
            // {isLoading !== 0 && <div className='pageloader'></div>}




        } else {
            console.log(
                "lsdkfj"
            )
        }
        setCallHistoryList(callHistoryList);
    }

    return (
        <>
            <CallHistoryCmp
                callHistoryList={callHistoryList}
            />
        </>
    )
}

export default CallHistory;