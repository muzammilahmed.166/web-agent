import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import ScheduleCmp from '../../../Components/DashboardCmp/CustomerQueue/ScheduleCmp';
import { getDashboardLiveSagaAction, getDashboardScheduleSagaAction } from '../../../Store/SagaActions/AgentDashboardSagaActions';
import { joinVideoSessionAction } from '../../../Store/SagaActions/InitiateVideoCallSagaActions';
import NoData from '../NoData';

const Schedule = () => {
    const [scheduleList, setScheduleList] = useState([]);
    const [ScheduleListData, setScheduleListData] = useState([]);

    const dispatch = useDispatch();
    const navigate = useNavigate();
    let intervalId;

    useEffect(() => {
        getLiveList();
        intervalId = setInterval(() => {
            getLiveList();
        }, 5000);
        return () => {
            clearIntervalLive();
        }
    }, []);

    const clearIntervalLive = () => {
        if (intervalId) {
            clearInterval(intervalId)
        }
    }

    const getLiveList = () => {
        const model = {
            listtype: '2' // -> 1-live, 2-scheduel
        }
        dispatch(getDashboardScheduleSagaAction({ model: model, callback: getLiveData }));
    }

    const getLiveData = (data) => {
        let ScheduleListData = []
        if (data?.length > 0) {
            data?.map((item, i) => {
                ScheduleListData.push({
                    "customer name": item?.name, "app id": item?.appid, "location": item?.customerloc, "ip status": item?.customeripstatus, "waiting since": item?.wtime,
                })

            })
            // {isLoading !== 0 && <div className='pageloader'></div>}




        } else {
            console.log(
                "lsdkfj"
            )

        }
        setScheduleList(data);
    }


    const joinVideoSession = (vcipData) => {
        const vcipID = vcipData?.videoconfsessionid;
        sessionStorage.setItem('vcipkey', vcipData?.vcipkey);
        const location = sessionStorage.getItem('location');
        const geolocation = sessionStorage.getItem('geolocation');
        const ip = sessionStorage.getItem('ip');
        const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
        const model = {
            "vcipkey": vcipData?.vcipkey,
            "custdetails": {
                "ip": "10.10.10.10",
                "location": "",
                "geolocation": "",
                "nw_incoming": "",
                "nw_outgoing": "",
                "videoresolution": ""
            },
            "agentdetails": {
                "ip": ip,
                "location": location,
                "geolocation": geolocation,
                "nw_incoming": connection.downlink,
                "nw_outgoing": `${connection.rtt}, ${connection.effectiveType === "4g" ? connection.effectiveType : "Low Speed"}`,
                "videoresolution": `${window.screen.width} * ${window.screen.height}`
            }

        }
        dispatch(joinVideoSessionAction({ model: model, callback: joinVideoSessionData, vcipID: vcipID }));
    }

    const joinVideoSessionData = (vcipID) => {
        navigate('/video/' + vcipID);
    }

    return (
        <>
            {/* {scheduleList && scheduleList?.length > 0
                ? <ScheduleCmp
                    scheduleList={scheduleList}
                    joinVideoSession={joinVideoSession}
                />
                : <NoData
                    title="No records found"
                    subtitle="No records found"
                    pageName="schedule"
                    img="../images/icon-paper.svg"
                />
            } */}
            <ScheduleCmp
                scheduleList={scheduleList}
                joinVideoSession={joinVideoSession}
            />
        </>
    )
}

export default Schedule;