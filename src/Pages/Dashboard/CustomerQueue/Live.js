import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import LiveCmp from '../../../Components/DashboardCmp/CustomerQueue/LiveCmp';
import StatusCodes from '../../../Constants/StatusCodes';
import { getDashboardLiveSagaAction } from '../../../Store/SagaActions/AgentDashboardSagaActions';
import { joinVideoSessionAction } from '../../../Store/SagaActions/InitiateVideoCallSagaActions';
import NoData from '../NoData';
import RouteNames from '../../../Constants/RouteNames';

const Live = () => {
    const [liveList, setLiveList] = useState([]);
    // const [isLoading, setIsLoading] = useState(false);

    const isLoading = useSelector(state => state.HomeReducer.apiStatus);



    // const [spinner, setSpinner] = useState(false);
    // const [vcipID, setVcipID] = useState('');

    const dispatch = useDispatch();
    const navigate = useNavigate();

    let intervalId;
    let vcipData;

    useEffect(() => {
        getLiveList();
        intervalId = setInterval(() => {
            getLiveList();
        }, 5000);
        return () => {
            clearIntervalLive();
        }
    }, []);

    const clearIntervalLive = () => {
        if (intervalId) {
            clearInterval(intervalId)
        }
    }

    const getLiveList = () => {
        const model = {
            listtype: '1' // -> 1-live, 2-scheduel
        }
        // setIsLoading(true);
        dispatch(getDashboardLiveSagaAction({ model: model, callback: getLiveData }));
    }

    const getLiveData = (data) => {
        // setLiveList(data);
        if (data?.respcode == "200") {

            let LiveListData = []
            if (data?.vciplist?.length > 0) {
                data?.vciplist?.map((item, i) => {
                    LiveListData.push({
                        "customer name": item?.name, "app id": item?.appid, "location": item?.customerloc, "ip status": item?.customeripstatus != "" ? <>
                            <span className="text-success">
                                <strong>
                                    {item?.customeripstatus.split("|")[0].trim()}
                                </strong>
                            </span>
                            <span>{item?.customeripstatus.split("|").slice(1)}</span>
                        </> : item?.customeripstatus
                        , "waiting since": item?.wtime, "status": item?.joinstatus === StatusCodes.VCIP_JOINSTATUS_ENABLE
                            ? <>
                                <button className='tab-btn-live' onClick={() => joinVideoSession(item)} ><i className="me-2 fa-light fa-phone"></i>Initiate call</button>
                            </>
                            : <button className='tab-btn-live disabled' disabled><i className="me-2 fa-light fa-phone"></i>Initiate call</button>

                    })
                    //         <button type="button" className="btn btn-primary w-20" data-bs-toggle="modal" data-bs-target="#viewKycReportModal">
                    //             View Kyc Report
                    //         </button>
                    // })
                })
                // {isLoading !== 0 && <div className='pageloader'></div>}




            } else {
                console.log(
                    "lsdkfj"
                )

            }
            setLiveList(LiveListData)
        } else if (data.respcode == "4003") {
            navigate(RouteNames.LOGIN);
            window.location.reload();
        }
    }

    const joinVideoSession = (vcipData) => {
        const vcipID = vcipData?.videoconfsessionid;
        sessionStorage.setItem('vcipkey', vcipData?.vcipkey);
        const location = sessionStorage.getItem('location');
        const geolocation = sessionStorage.getItem('geolocation');
        const ip = sessionStorage.getItem('ip');
        const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
        const model = {
            "vcipkey": vcipData?.vcipkey,
            "custdetails": {
                "ip": "10.10.10.10",
                "location": "",
                "geolocation": "",
                "nw_incoming": "",
                "nw_outgoing": "",
                "videoresolution": ""
            },
            "agentdetails": {
                "ip": ip,
                "location": location,
                "geolocation": geolocation,
                "nw_incoming": connection.downlink,
                "nw_outgoing": `${connection.rtt}, ${connection.effectiveType === "4g" ? connection.effectiveType : "Low Speed"}`,
                "videoresolution": `${window.screen.width} * ${window.screen.height}`
            }

        }
        // setIsLoading(true);
        dispatch(joinVideoSessionAction({ model: model, callback: joinVideoSessionData, vcipID: vcipID }));
    }

    const joinVideoSessionData = (vcipID) => {
        // setIsLoading(true);
        navigate('/video/' + vcipID);
    }

    return (
        <>
            {/* {liveList && liveList?.length > 0
                ? <LiveCmp
                    liveList={liveList}
                    joinVideoSession={joinVideoSession}
                />
                : <NoData
                    title="Sit back and relax!"
                    subtitle="There are currently no live customers in the queue"
                    img="../images/icon-sleepy.svg"
                    pageName="live"
                />
            } */}
            <LiveCmp
                liveList={liveList}
                joinVideoSession={joinVideoSession}
            />

        </>
    )
}

export default Live;