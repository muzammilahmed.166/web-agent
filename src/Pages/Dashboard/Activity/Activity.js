import React, { useEffect, useState } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import { getDashboardDataSagaAction } from '../../../Store/SagaActions/AgentDashboardSagaActions';
import { useDispatch } from 'react-redux';

ChartJS.register(ArcElement, Tooltip, Legend);

const Activity = () => {

    const [dashboardData, setDashboardData] = useState({});
    const [chartData, setChartData] = useState({
        labels: ['Rejected', 'issued', 'Accepted'],
        datasets: [
            {
                label: '# of Votes',
                data: [2, 1, 2],
                backgroundColor: [
                    '#4DAE68',
                    '#F9C717',
                    '#F24747',
                ],
                borderWidth: 1,
            },
        ],
    });

    const dispatch = useDispatch();

    useEffect(() => {
        getDashboardData('0');
    }, [])

    const getDashboardData = (type) => {
        const model = {
            reqtype: type
        }
        dispatch(getDashboardDataSagaAction({ model: model, callback: getDashboardDatares }));
    }

    const getDashboardDatares = (data) => {
        // const countArr = [data?.rejectedcount !== "0" ? data?.rejectedcount : 2, data?.issuedcount !== "0" ? data?.issuedcount : 3, data?.acceptedcount]
        const countArr = [data?.rejectedcount, data?.issuedcount, data?.acceptedcount]
        const chartObj = {
            labels: ['Rejected', 'Issued', 'Accepted'],
            datasets: [
                {
                    label: '# of Votes',
                    data: countArr,
                    backgroundColor: [
                        '#F24747',
                        '#F9C717',
                        '#4DAE68',
                    ],
                    borderWidth: 1,
                },
            ],
        };
        setDashboardData(data);
        setChartData(chartObj);
    }

    const changeActivity = (event) => {
        const { value } = event.target;
        getDashboardData(value);
    }

    return (
        <>

            <div className="bx-hdr">
                <h5 className="bx-hdr-ttl">My Activity</h5>
                <div>
                    <label className="bx-hdr-lbl">Show:</label>
                    <select
                        name="show"
                        className="bx-hdr-inp"
                        onChange={changeActivity}>
                        <option value="0">Today</option>
                        <option value="1">Overall</option>
                    </select>
                </div>
            </div>
            <div className="bx-cll-chrt mt-4">
                <div className='ttl-cnt'>
                    <h4 className='ttl-cnt-no'>{parseInt(dashboardData?.acceptedcount) + parseInt(dashboardData?.rejectedcount) + parseInt(dashboardData?.issuedcount)}</h4>
                    <p className='ttl-cnt-txt'>Total Calls</p>
                </div>
                <Doughnut data={chartData} />
            </div>
            <ul className="bx-cll-lst">
                <li className="bx-cll-lst-itm">
                    <p className="bx-cll-lst-itm-nm">
                        <span className="itm-green" />
                        Approved
                    </p>
                    <span className="bx-cll-lst-itm-cnt">{dashboardData?.acceptedcount}</span>
                </li>
                <li className="bx-cll-lst-itm">
                    <p className="bx-cll-lst-itm-nm">
                        <span className="itm-red" />
                        Rejected
                    </p>
                    <span className="bx-cll-lst-itm-cnt">{dashboardData?.rejectedcount}</span>
                </li>
                <li className="bx-cll-lst-itm">
                    <p className="bx-cll-lst-itm-nm">
                        <span className="itm-yellow" />
                        Issue
                    </p>
                    <span className="bx-cll-lst-itm-cnt">{dashboardData?.issuedcount}</span>
                </li>
            </ul>
        </>
    )
}

export default Activity