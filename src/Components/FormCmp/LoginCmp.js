import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import RouteNames from '../../Constants/RouteNames'

const LoginCmp = (props) => {

    const [eyeBlinklog, setEyeBlinklog] = useState({ one: false, two: false, three: false });

    const handleChangePasswordTypelog = (value) => {
        console.log(value)
        setEyeBlinklog(prev => ({ ...prev, [value]: !prev[value] }))
    }
    return (
        <>
            <form className="lgn-rt-frm" onSubmit={props?.login}>
                <h1 className="lgn-rt-frm-ttl">Hello Again! 👋🏼</h1>
                <p className="lgn-rt-frm-txt">Welcome Back</p>
                {/* Tal change starts */}
                <div class="input-group mb-3">
                    <span class="input-group-text cus-logIcon" id="one"><i class="fa-regular fa-md fa-envelope"></i>
                    </span>
                    <input
                        type="email"
                        name="username"
                        onChange={props?.handleChange}
                        className="form-control cus-logIn"
                        placeholder="Email Address"
                        required />
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text cus-logIcon" id="one"><i class="fa-regular fa-md fa-lock-keyhole"></i>
                    </span>
                    <input
                        type={`${eyeBlinklog.two ? "text" : "password"}`}
                        name="password"
                        class="form-control cus-logIn2"
                        onChange={props?.handleChange}
                        placeholder="Password"
                        required />
                    <span class="input-group-text cus-logIcon" id="two" onClick={() => handleChangePasswordTypelog("two")}>
                        {
                            eyeBlinklog.two ? <i class="fa-light fa-lg fa-eye"></i> : <i class="fa-solid fa-eye-slash"></i>
                        }
                    </span>
                </div>
                {/* Tal changes stop */}
                {/* <div className="frm-grp">
                    <input
                        type="email"
                        name="username"
                        onChange={props?.handleChange}
                        className="frm-grp-inp"
                        placeholder="Email Address"
                        required />
                </div>
                <div className="frm-grp">
                    <input
                        type="password"
                        name="password"
                        onChange={props?.handleChange}
                        className="frm-grp-inp"
                        placeholder="Password"
                        required />
                </div> */}

                {/* <div className="frm-grp">
                <select
                    name='rolename'
                    onChange={props?.handleChange}
                    className="frm-grp-inp"
                    required>
                    {props?.roles && props?.roles?.length > 0
                        ? props?.roles.map((item, i) => <option key={i} value={item?.rid + '-' + item?.rolename}>{item?.rolename}</option>)
                        : (null)
                    }
                </select>
            </div> */}
                <div className="text-center">
                    <button className="btn" type="submit" >Login {props.isLoading && <span className="spinner">{console.log("text")}</span>}</button>

                    {/* <button className="btn" type="submit" className ="exampleModal" data-bs-modal ="modal"
         type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#loginModalByAgent">Login</button> */}
                </div>
                <p className="text-center mb-0 mt-2">
                    <Link to={RouteNames.RESET} className="lgn-link">Forgot Password?</Link>
                </p>
            </form>



            <div className="modal fade" id="login" data-backdrop="static" tabIndex={-1} role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" style={{ maxWidth: "auto" }} role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            <div className="instructions pt-4 border-right-0">
                                <form onSubmit={(event) => props?.login(event, '1')} >

                                    <h6 className="instructions-title text-center fs-3">
                                        Already Logged in Another Session/Browser. Are you sure want to clear all the other sessions?
                                    </h6>
                                    <div className="modal-footer justify-content-center">
                                        <button type="button" className="btn w-25 btn-white" data-dismiss="modal" onClick={props?.validationyes}>Yes</button>
                                        <button type="button" className="btn w-25" onClick={props?.validationno}>NO</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    )
}

export default LoginCmp