import React from 'react'
import { Link } from 'react-router-dom';
import RouteNames from '../../Constants/RouteNames';

const ChangePasswordCmp = (props) => {
    return (
        <form className="lgn-rt-frm" onSubmit={props?.changepassword}>
            <h1 className="lgn-rt-frm-ttl">Reset password 🔐</h1>
            <p className="lgn-rt-frm-txt">Please enter your password</p>
            <div className="frm-grp">
                <input type="password"
                    name="newpassword" className="frm-grp-inp" placeholder="New Password" onChange={props?.handleChange}
                    required />
            </div>
            <div className="frm-grp">
                <input type="password"
                    name="retypepassword" className="frm-grp-inp" placeholder="Retype Password" onChange={props?.handleChange}
                    required />
            </div>
            <div className="text-center">
                <button className="btn" type="submit" data-toggle="modal" data-target="#reset">Change</button>
            </div>
            <p className="text-center mb-0 mt-2">
                <Link to={RouteNames.LOGIN} className="lgn-link">Login</Link>
            </p>
        </form>

    )
}

export default ChangePasswordCmp;