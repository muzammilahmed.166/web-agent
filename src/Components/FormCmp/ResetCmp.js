import React from 'react'
import { Link } from 'react-router-dom';
import RouteNames from '../../Constants/RouteNames';

const ResetCmp = (props) => {
    return (
        <form className="lgn-rt-frm" onSubmit={props?.reset}>
            <h2 className="lgn-rt-frm-ttl">Reset Password 🔐</h2>
            <p className="lgn-rt-frm-txt">Please enter your email address</p>
            
            <div class="input-group mb-3">
                    <span class="input-group-text cus-logIcon" id="one"><i class="fa-regular fa-md fa-envelope"></i>
                    </span>
                    <input
                        type="email"
                        name="username"
                        onChange={props?.handleChange}
                        className="form-control cus-logIn"
                        placeholder="Email Address"
                        required />
                </div>
            {/* <div className="frm-grp">
                <input type="email" name="email" className="frm-grp-inp" placeholder="Email Address" onChange={props?.handleChange}
                    required />
            </div> */}
            <div className="text-center">
                <button className="btn" type="submit" data-toggle="modal" data-target="#reset">Submit</button>
            </div>
            <p className="text-center mb-0 mt-2">
                <Link to={RouteNames.LOGIN} className="lgn-link">Login</Link>
            </p>
        </form>

    )
}

export default ResetCmp;