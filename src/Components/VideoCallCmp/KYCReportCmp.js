import React, { useState } from 'react';

const KYCReportCmp = (props) => {
    let reportData = props?.customerVcipDetails;
    return (
        <>
             <div className="container-fluid kyc-reportpage">
                 <div>
                 <h5 className="modal-title m-auto text-center" id="exampleModalLabel">
                    KYC Report </h5>
            </div>
                <div className="row ">
                    <div className="col-lg-12">


                        <table className="table table-borderless">
                        <thead>
                                    <tr>
                                        <th colSpan={5} className="text-center">
                                            <h3 className="customer-heading">Customer details</h3>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th className="kyc-header">User details</th>
                                        <th className="kyc-header">Applicant from data</th>
                                        <th className="kyc-header">Aadhaar data <button className="btn btn-success btn-circle btn-circle-sm m-1">
                                            <img src="../images/Approve(Tick).svg" alt="" />
                                        </button></th>
                                        <th className="kyc-header">PAN details<button className="btn btn-success btn-circle btn-circle-sm m-1">
                                            <img src="../images/Approve(Tick).svg" alt="" />
                                        </button></th>
                                        <th className="kyc-header">Match</th>
                                    </tr>
                                </thead>

                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{reportData?.customerdetails?.name ? reportData?.customerdetails?.name : "N/A"}</td>
                                    <td>{reportData?.kycdetails?.name ? reportData?.kycdetails?.name : "N/A"}</td>
                                    <td>{reportData?.pandetails?.ainame ? reportData?.pandetails?.ainame : "N/A"}</td>
                                    <td>{reportData?.fuzzymatchdetails?.kyc_pan_name ? reportData?.fuzzymatchdetails?.kyc_pan_name > '50%' ? <span className="text-success">{reportData?.fuzzymatchdetails?.kyc_pan_name}</span> : <span className="text-danger">{reportData?.fuzzymatchdetails?.kyc_pan_name}</span> : "N/A"}</td>
                                </tr>

                                <tr>
                                    <td>Father’s name</td>
                                    <td>{reportData?.customerdetails?.fname ? reportData?.customerdetails?.fname : "N/A"}</td>
                                    <td>{reportData?.kycdetails?.fname ? reportData?.kycdetails?.fname : "N/A"}</td>
                                    <td>{reportData?.pandetails?.aifname ? reportData?.pandetails?.aifname : "N/A"}</td>
                                    <td>{reportData?.fuzzymatchdetails?.kyc_pan_fname ? reportData?.fuzzymatchdetails?.kyc_pan_fname > '50%' ? <span className="text-success">{reportData?.fuzzymatchdetails?.kyc_pan_fname}</span> : <span className="text-danger">{reportData?.fuzzymatchdetails?.kyc_pan_fname}</span> : "N/A"}</td>
                                </tr>

                                <tr>
                                    <td>Date of Birth</td>
                                    <td>{reportData?.customerdetails?.dob ? reportData?.customerdetails?.dob : "N/A"}</td>
                                    <td>{reportData?.kycdetails?.dob ? reportData?.kycdetails?.dob : "N/A"}</td>
                                    <td>{reportData?.pandetails?.aidob ? reportData?.pandetails?.aidob : "N/A"}</td>
                                    <td>{reportData?.fuzzymatchdetails?.kyc_pan_dob ? reportData?.fuzzymatchdetails?.kyc_pan_dob > '50%' ? <span className="text-success">{reportData?.fuzzymatchdetails?.kyc_pan_dob}</span> : <span className="text-danger">{reportData?.fuzzymatchdetails?.kyc_pan_dob}</span> : "N/A"}</td>
                                </tr>

                                <tr>
                                    <td>
                                        Gender
                                    </td>
                                    <td>{reportData?.customerdetails?.gender ? reportData?.customerdetails?.gender : "N/A"}</td>
                                    <td>{reportData?.kycdetails?.gender ? reportData?.kycdetails?.gender : "N/A"}</td>
                                    <td>-</td>
                                    <td>{reportData?.fuzzymatchdetails?.kyc_gender ? parseInt(reportData?.fuzzymatchdetails?.kyc_gender) > parseInt('50%') ? <span className="text-success">{reportData?.fuzzymatchdetails?.kyc_gender}</span> : <span className="text-danger">{reportData?.fuzzymatchdetails?.kyc_gender}</span> : "N/A"}</td>

                                </tr>
                                <tr>
                                    <td>
                                        Current address
                                    </td>
                                    <td>{reportData?.customerdetails?.curr_address ? reportData?.customerdetails?.curr_address : "N/A"}</td>
                                    <td>{reportData?.kycdetails?.address ? reportData?.kycdetails?.address : "N/A"}</td>
                                    <td>-</td>
                                    <td>{reportData?.fuzzymatchdetails?.kyc_curr_address ? reportData?.fuzzymatchdetails?.kyc_curr_address > '50%' ? <span className="text-success">{reportData?.fuzzymatchdetails?.kyc_curr_address}</span> : <span className="text-danger">{reportData?.fuzzymatchdetails?.kyc_curr_address}</span> : "N/A"}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Permanent address                                    </td>
                                    <td>{reportData?.customerdetails?.per_address ? reportData?.customerdetails?.per_address : "N/A"}</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>{reportData?.fuzzymatchdetails?.kyc_per_address ? reportData?.fuzzymatchdetails?.kyc_per_address > '50%' ? <span className="text-success">{reportData?.fuzzymatchdetails?.kyc_per_address}</span> : <span className="text-danger">{reportData?.fuzzymatchdetails?.kyc_per_address}</span> : "N/A"}</td>
                                </tr>
                                <tr>
                                    <td>
                                        Mobile number                                   </td>
                                    <td>{reportData?.customerdetails?.mobile ? reportData?.customerdetails?.mobile : "N/A"}</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>
                                        Email address                                   </td>
                                    <td>{reportData?.customerdetails?.email ? reportData?.customerdetails?.email : "N/A"}</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                        </table>
                        {/* <table cl */}
                    </div>
                    
                    <div className="col-lg-4 mt-1 ">
                        <div className="face-box">
                            <h6 className="text-center">liveliness check</h6>
                            <hr></hr>
                            <div className="text-center">
                            <img className ="w-50" src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="live photo" />
                            </div>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    {/* <h5>Match score - <b className="text-success">99.9% </b></h5> */}
                                    <h6>Match score - {reportData?.livecapturedetails?.livecapturepht_matchlevel ? reportData?.livecapturedetails?.livecapturepht_matchlevel > '50' ? <span className ="text-success">{reportData?.livecapturedetails?.livecapturepht_matchlevel}%</span>:<span className ="text-danger">{reportData?.livecapturedetails?.livecapturepht_matchlevel}%</span> : 'N/A'} </h6>

                                </div>
                                <div className="col-lg-6 text-center">
                                    <h6>Status - {reportData?.livecapturedetails?.livecapturestatus ? (reportData?.livecapturedetails?.livecapturestatus === 1) ? <span className ="text-success">Yes</span>:<span className ="text-danger">No</span>: 'N/A'}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 mt-1 ">
                        <div className="face-box">
                            <h6 className="text-center">Face match with Aadhaar</h6>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                <img src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="live photo" className ="w-100" />
                                </div>
                                <div className="col-lg-6 text-center">
                                <img src={"data:image/png;base64," + reportData?.kycdetails?.pht} alt="" className ="w-100" />                                </div>
                            </div>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    {/* <h5>Match score - <b className="text-success">99.9% </b></h5> */}
                                    <h6>Match score -  {reportData?.live_aadhaar_pht_matchlevel ? reportData?.live_aadhaar_pht_matchlevel > '50' ? <span className ="text-success">{reportData?.live_aadhaar_pht_matchlevel}%</span>:<span className ="text-danger">{reportData?.live_aadhaar_pht_matchlevel}%</span> : 'N/A'}</h6>

                                </div>
                                <div className="col-lg-6 text-center">
                                    <h6>Status - {reportData?.livecapturedetails?.livecapturestatus ? (reportData?.livecapturedetails?.livecapturestatus === 1) ? <span className ="text-success">Yes</span>:<span className ="text-danger">No</span>: 'N/A'}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 mt-1">
                        <div className="face-box">
                            <h6 className="text-center">Face match with PAN</h6>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                <img src={"data:image/png;base64," + reportData?.livecapturedetails?.livecapturepht} alt="live photo" className ="w-100"/>                                </div>
                                <div className="col-lg-6 text-center">
                                <img src={"data:image/png;base64," + reportData?.pandetails?.pancard} alt=""  className="w-100"/>                            </div>
                            <hr></hr>
                            <div className="row">
                                <div className="col-lg-6 text-center">
                                    {/* <h5>Match score - <b className="text-success">99.9% </b></h5> */}
                                    <h6>Match score -  {reportData?.live_pan_pht_matchstatus ? reportData?.live_pan_pht_matchstatus > '50' ? <span className ="text-success">{reportData?.live_pan_pht_matchstatus}%</span>:<span className ="text-danger">{reportData?.live_pan_pht_matchstatus}%</span> : 'N/A'}</h6>

                                </div>
                                <div className="col-lg-6 text-center">
                                    <h6>Status - {reportData?.live_pan_pht_matchlevel ? reportData?.live_pan_pht_matchlevel === 1 ? <span className ="text-success">Yes</span>:<span className ="text-danger">No</span> : 'N/A'}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                    

                </div>

               
                <div className="match-score mt-5">
                            <h5>Mark the status of the application</h5>
                            {!props?.isMatchChecked && <>
                                <button className="btn-round" onClick={() => props?.selectedStatus("3")}>
                                    <img src='../../images/icon-wrong.svg' alt="check" />
                                </button>
                                <button className="btn-round btn-green" onClick={() => props?.selectedStatus("2")}>
                                    <img src='../../images/icon-check.svg' alt="check" />
                                </button>
                            </>}
                            {console.log(props?.isSelected,"selected")}
                            {props?.isMatchChecked ? (<button className={`btn-round ${props?.isSelected === false ? '' : 'btn-green'}`}>
                                <img src={`${props?.isSelected === false ? '../../images/icon-wrong.svg' : '../../images/icon-check.svg'}`} alt="check" />
                            </button>)
                                : (null)
                            }
                        </div>
                        <div className="form-group p-3 group-form">
                            <textarea
                                className="form-control add-remark w-100"
                                onChange={props?.handleRemorks}
                                maxLength={500}
                                placeholder="Add remarks (optional)"
                            ></textarea>
                        </div>

                        <div className='text-center mb-1'>
                            <button
                                onClick={props?.submitKyc}
                                type="button"
                                className="btn w-25"
                                data-toggle="modal"
                                data-target="#examplemodelfacematch">
                                Submit {props.isLoading ? <span className ="spinner mr-2"></span>:null}
                            </button>
                        </div>
        </div>

        </>
    )
}

export default KYCReportCmp;