import React from 'react'

const AadharCheckCmp = (props) => {
    return (
        <>
            <article className='prt-1'>
                <h5>Aadhaar offline KYC</h5>
                <hr className="hr-horizontal" />
                <div className="row">
                    <div className="col-md-6">
                        <div className="aadhaar-offline-kyc">
                            <img
                                src={props?.customerVcipDetails?.kycdetails?.pht
                                    ? "data:image/png;base64," + props?.customerVcipDetails?.kycdetails?.pht
                                    : ''} alt="" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="aadhaar-offline-kyc">
                            <img
                                src={props?.img ? props?.img : ''} alt="" />
                        </div>
                    </div>
                </div>
                <div className="match-score mt-4">
                    {/* <h5>Match score - {props?.aadhaarOfflineMatchData?.confidence ? props?.aadhaarOfflineMatchData?.confidence : '-'}</h5> */}
                    <h5>Face Match score - </h5>
                    {props?.facematchDetails?.confidence ? props?.facematchDetails?.confidence > '50' ? <span className="text-success faceliveness-score">{props?.facematchDetails?.confidence}%</span> : <span className="text-danger faceliveness-score">{props?.facematchDetails?.confidence}%</span> : '-'}
                </div>
                <div className="match-score">
                    <h6 className="match-condition">Does the face match with aadhaar photo?</h6>
                    {!props?.isMatchChecked && <>
                        <button className="btn-round" onClick={() => props?.updateFacematchStatus("0")}>
                            <img src='../../images/icon-wrong.svg' alt="check" />
                        </button>
                        <button className="btn-round btn-green" onClick={() => props?.updateFacematchStatus("1")}>
                            <img src='../../images/icon-check.svg' alt="check" />
                        </button>
                    </>}
                    {props?.isMatchChecked ? (<button className={`btn-round ${props?.isSelected === '0' ? '' : 'btn-green'}`}>
                        <img src={`${props?.isSelected === '0' ? '../../images/icon-wrong.svg' : '../../images/icon-check.svg'}`} alt="check" />
                    </button>)
                        : (null)
                    }
                </div>
                <div className="form-group group-form">
                    {/* <input type="text" className="form-control add-remark" placeholder="Add remarks (optional)" defaultValue /> */}
                    <input
                        className="form-control w-100 m-0 p-2"
                        // onChange={handleRemorks}
                        maxLength={100}
                        placeholder="Add remarks (optional)"
                        required
                        />

                </div>


                {/* {props?.isMatchChecked &&
                    <div style={{ overflowX: 'auto' }} className="table-customer-details">
                        <table className="table kycdetails">
                            <thead>
                                <tr className='vertical-align-middle'>
                                    <th className="kyc-header">User details</th>
                                    <th className="kyc-header">Applicant from data</th>
                                    <th className="kyc-header">Aadhaar data</th>
                                    <th className="kyc-header">Match</th>
                                </tr>
                            </thead>
                            <tbody className="table-userdetails">
                                <tr>
                                    <td className="kyc-reportdataheader">
                                        <p>Name</p>
                                        <p>Father’s name</p>
                                        <p>DOB</p>
                                        <p>Gender</p>
                                        <p>Current
                                            address</p>
                                        <p>Permanent address</p>
                                        <p>Mobile number</p>
                                        <p>Email address</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.name ? props?.customerVcipDetails?.kycdetails?.name : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.fname ? props?.customerVcipDetails?.kycdetails?.fname : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.dob ? props?.customerVcipDetails?.kycdetails?.dob : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.gender ? props?.customerVcipDetails?.kycdetails?.gender : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.address ? props?.customerVcipDetails?.kycdetails?.address : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.kycdetails?.address ? props?.customerVcipDetails?.kycdetails?.address : "-"}</p>
                                        <p className="kyc-data">-</p>
                                        <p className="kyc-data">-</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.name ? props?.customerVcipDetails?.customerdetails?.name : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.fname ? props?.customerVcipDetails?.customerdetails?.fname : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.dob ? props?.customerVcipDetails?.customerdetails?.dob : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.gender ? props?.customerVcipDetails?.customerdetails?.gender : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.curr_address ? props?.customerVcipDetails?.customerdetails?.curr_address : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.curr_address ? props?.customerVcipDetails?.customerdetails?.curr_address : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.mobile ? props?.customerVcipDetails?.customerdetails?.mobile : "-"}</p>
                                        <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.email ? props?.customerVcipDetails?.customerdetails?.email : "-"}</p>
                                    </td>
                                    <td className="kyc-reportdata">
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_name : "-"}</p>
                                        <p className="kyc-match text-danger"> {props?.customerVcipDetails?.fuzzymatchdetails?.kyc_fname ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_fname : "-"}</p>
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_dob : "-"}</p>
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_gender : "-"}</p>
                                        <p className="kyc-match text-warning">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_curr_address : "-"}</p>
                                        <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_per_address : "-"}</p>
                                        <p className="kyc-match text-success">-</p>
                                        <p className="kyc-match text-success">-</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="pan-status-result">
                            <h4>XML Generation date:</h4>
                            <span>-</span>
                            <h5> {props?.customerVcipDetails?.kycdetails?.offlinekycgendate}</h5>
                            <img src="../../images/Vector-solid-right.svg" alt="" />
                        </div>
                    </div>
                } */}

            </article>
            <article className='prt-2'>
                {console.log(props?.disabledBtn, 'adf')}
                <button className="btn" onClick={props?.nextPage} disabled={props?.disabledBtn}>Next </button>
            </article>
        </>
    )
}

export default AadharCheckCmp;