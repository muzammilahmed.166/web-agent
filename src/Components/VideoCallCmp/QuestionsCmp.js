import React from 'react'

const QuestionsCmp = (props) => {
    return (
        <>
            <article className='prt-1'>
                <h5>Q & A</h5>
                <hr className="hr-horizontal" />
                {props?.questionsList?.length > 0
                    ? props?.questionsList?.map((item, i) => <div className='qtn-bx' key={i}>
                        <div className="qtn-bx-ch">
                            <div className='qtn-bx-ch-l'>
                                <h6 className="qtn-name">Q. {item?.ques}</h6>
                                <h6 className="qtn-name">A. {item?.answer}</h6>
                            </div>
                            <div className='qtn-bx-ch-r'>
                                {/* {item?.isAnwsered
                                    ? <button className={`btn ${item?.status === '0' ? 'btn-red' : 'btn-green'}`}>Correct</button>
                                    : <>
                                        <button className="btn-round" onClick={() => props?.askQuestion(item, "0")}>X</button>
                                        <button className="btn-round btn-green" onClick={() => props?.askQuestion(item, "1")}>R</button>
                                    </>
                                } */}

                                {!item?.isAsk
                                    ? <button className="btn" onClick={() => props?.askQuestion(item)}>Ask</button>
                                    : item?.isAnwsered
                                        ? <button className={`btn ${item?.status === '0' ? 'btn-red' : 'btn-green'}`}>{item?.status === '0' ? 'Incorrect' : 'Correct'}</button>
                                        : <>
                                            <button className="btn-round" onClick={() => props?.submitQuestion(item, "0")}>
                                                <img src='../../images/icon-wrong.svg' alt="check" />
                                            </button>
                                            <button className="btn-round btn-green" onClick={() => props?.submitQuestion(item, "1")}>
                                                <img src='../../images/icon-check.svg' alt="check" />
                                            </button>
                                        </>
                                }

                            </div>
                        </div>
                        <div className="frm-grp mb-1">
                            <input
                                type="text"
                                name=""
                                defaultValue={props?.remarks}
                                disabled={item?.isAnwsered}
                                onChange={(event) =>props?.handleRemorks(event, item, i)}
                                className='frm-grp-inp'
                                placeholder='Add remarks (optional)'
                            />
                        </div>
                    </div>)
                    : (null)
                }
                {/* <div className='qtn-bx'>
                    <div className="qtn-bx-ch">
                        <div className='qtn-bx-ch-l'>
                            <h6 className="qtn-name">Q. What is your father’s name?</h6>
                            <h6 className="qtn-name">A. Ragavendra Rao</h6>
                        </div>
                        <div className='qtn-bx-ch-r'>
                            <button className="btn btn-green">Correct</button>
                        </div>
                    </div>
                    <div className="frm-grp mb-1">
                        <input
                            type="text"
                            name=""
                            className='frm-grp-inp'
                            placeholder='Add remarks (optional)'
                        />
                    </div>
                </div>
                <div className='qtn-bx'>
                    <div className="qtn-bx-ch">
                        <div className='qtn-bx-ch-l'>
                            <h6 className="qtn-name">Q. What is your father’s name?</h6>
                            <h6 className="qtn-name">A. Ragavendra Rao</h6>
                        </div>
                        <div className='qtn-bx-ch-r'>
                            <button className="btn-round">X</button>
                            <button className="btn-round btn-green">R</button>
                        </div>
                    </div>
                    <div className="frm-grp mb-0">
                        <input
                            type="text"
                            name=""
                            className='frm-grp-inp'
                            placeholder='Add remarks (optional)'
                        />
                    </div>
                </div> */}
            </article>
            <article className='prt-2'>
                <button className="btn" onClick={props?.nextPage} disabled ={ props?.isDisabled}

>Next</button>
            </article>
        </>
    )
}

export default QuestionsCmp;