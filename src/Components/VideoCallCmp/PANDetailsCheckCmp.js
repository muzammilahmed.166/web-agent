import React from 'react'

const PANDetailsCheckCmp = (props) => {
    return (
        <>
            <article className='prt-1'>
                <h5>PAN Details Check</h5>
                <hr className="hr-horizontal" />
                <div style={{ overflowX: 'auto' }} className="table-customer-details">
                    
                    <table className="table kycdetails table-borderless">
                    <thead>
                            <tr className='vertical-align-middle'>
                                <th className="kyc-header vertical-align-middle">User details</th>
                                <th className="kyc-header ">Applicant from data</th>
                                <th className="kyc-header">OCR output</th>
                                <th className="kyc-header">Match</th>
                            </tr>
                        </thead>

                        <tbody className='table-userdetails'>
                            <tr>
                                <td> Name</td>
                                <td> {props?.customerVcipDetails?.customerdetails?.name ? props?.customerVcipDetails?.customerdetails?.name : "-"}</td>
                                <td> {props?.customerVcipDetails?.pandetails?.ainame ? props?.customerVcipDetails?.pandetails?.ainame : "-"}</td>
                                <td> {props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name}</span> : "-"}</td>
                            </tr>
                            <tr>
                                <td> Father’s name</td>
                                <td >{props?.customerVcipDetails?.customerdetails?.fname ? props?.customerVcipDetails?.customerdetails?.fname : "-"}</td>
                                <td >{props?.customerVcipDetails?.pandetails?.aifname ? props?.customerVcipDetails?.pandetails?.aifname : "-"}</td>
                                <td > {props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname}</span> : "-"}</td>
                            </tr>
                            <tr>
                                <td> DOB</td>
                                <td >{props?.customerVcipDetails?.customerdetails?.dob ? props?.customerVcipDetails?.customerdetails?.dob : "-"}</td>
                                <td >{props?.customerVcipDetails?.pandetails?.aidob ? props?.customerVcipDetails?.pandetails?.aidob : "-"}</td>
                                <td >{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob}</span> : "-"}</td>
                            </tr>
                            <tr>
                                <td> PAN Number</td>
                                <td >{props?.customerVcipDetails?.customerdetails?.oemstatus ? props?.customerVcipDetails?.customerdetails?.oemstatus : "-"}</td>
                                <td >{props?.customerVcipDetails?.pandetails?.aipannumber ? props?.customerVcipDetails?.pandetails?.aipannumber : "-"}</td>
                                <td >{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber ? props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber}</span> : "-"}</td>
                            </tr>
                        </tbody>
                    </table>
                    {/* <table className="table kycdetails">
                        <thead>
                            <tr className='vertical-align-middle'>
                                <th className="kyc-header vertical-align-middle">User details</th>
                                <th className="kyc-header ">Applicant from data</th>
                                <th className="kyc-header">OCR output</th>
                                <th className="kyc-header">Match</th>
                            </tr>
                        </thead>

                       
                        <tbody className="table-userdetails">
                            <tr>
                                <td className="kyc-reportdataheader">
                                    <p>Name</p>
                                    <p>Father’s name</p>
                                    <p>DOB</p>
                                    <p>PAN Number</p>
                                </td>
                                <td className="kyc-reportdata">
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.name ? props?.customerVcipDetails?.customerdetails?.name : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.fname ? props?.customerVcipDetails?.customerdetails?.fname : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.dob ? props?.customerVcipDetails?.customerdetails?.dob : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.customerdetails?.oemstatus ? props?.customerVcipDetails?.customerdetails?.oemstatus : "-"}</p>
                                </td>

                                <td className="kyc-reportdata">
                                    <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.ainame ? props?.customerVcipDetails?.pandetails?.ainame : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.aifname ? props?.customerVcipDetails?.pandetails?.aifname : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.aidob ? props?.customerVcipDetails?.pandetails?.aidob : "-"}</p>
                                    <p className="kyc-data">{props?.customerVcipDetails?.pandetails?.aipannumber ? props?.customerVcipDetails?.pandetails?.aipannumber : "-"}</p>
                                </td>
                                <td className="kyc-reportdata">
                                    <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_name}</span> : "-"}</p>
                                    <p className="kyc-match text-danger"> {props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_fname}</span> : "-"}</p>
                                    <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob ? props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.kyc_pan_dob}</span> : "-"}</p>
                                    <p className="kyc-match text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber ? props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber > '50%' ? <span className ="text-success">{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber}</span>:<span className ="text-danger">{props?.customerVcipDetails?.fuzzymatchdetails?.pan_pannumber}</span> : "-"}</p>
                                </td>

                            </tr>
                        </tbody>
                    </table> */}
                    <div className="pan-status-result">
                        <h4>XML Generation date:</h4>
                        <span>-</span>
                        <h5> {props?.customerVcipDetails?.kycdetails?.offlinekycgendate}</h5>
                        <img src="../../images/Vector-solid-right.svg" alt="" />
                    </div>
                </div>
            </article>
            <article className='prt-2'>
                <button className="btn" onClick={props?.nextPage}>End Call {props.isLoading ? <span className="spinner"> </span>: null}</button>
            </article>
        </>
    )
}

export default PANDetailsCheckCmp;